#!/bin/bash

#SBATCH --job-name=etx4velo
#SBATCH --nodes=1
#SBATCH --time=28800
#SBATCH --output=%x-%j.out
#SBATCH --error=%x-%j.err
#SBATCH --gpus=a100_3g.40gb:1
#SBATCH --cpus-per-gpu=16

eval "$(conda shell.bash hook)"
source setup/setup.sh
conda activate etx4velo_env
cd etx4velo/notebooks
# Convert notebook to python script, filter out magic commands, and run
jupyter nbconvert --to script full_pipeline.ipynb --stdout | grep -v -e "^get_ipython" | python
