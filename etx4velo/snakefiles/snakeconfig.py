"""A module that loads the snakemake configuration.
"""
import typing
import os.path as op
import yaml
from utils.commonutils.config import cdirs
from utils.commonutils.ctests import get_preprocessed_test_dataset_dir


def str_to_list(element: str | typing.List[str]) -> typing.List[str]:
    if isinstance(element, str):
        return [element]
    else:
        assert isinstance(element, list)
        return element


# Load configuration file
with open(op.join(op.dirname(__file__), "snakeconfig.yaml"), "r") as snakeconfig_file:
    snakeconfig = yaml.load(snakeconfig_file, Loader=yaml.SafeLoader)

# Make sure the "steps" of ever experiment is a list
for experiment_name, experiment_config in snakeconfig["experiments"].items():
    if experiment_config["steps"] is not None:
        experiment_config["steps"] = str_to_list(experiment_config["steps"])


STEPS = ["embedding", "gnn", "track_building"]

TEST_CONFIG_PATH = cdirs.test_config_path

DETECTOR_TO_PARQUET_FILE_NAMES = {
    "velo": ["mc_particles.parquet.lz4", "hits_velo.parquet.lz4"],
    "scifi": ["mc_particles.parquet.lz4", "hits_scifi.parquet.lz4"],
    "scifi_xz": ["mc_particles.parquet.lz4", "hits_scifi.parquet.lz4"],
}

preprocessed_test_wdir = get_preprocessed_test_dataset_dir(
    test_dataset_name="{test_dataset_name}", detector="{detector}"
)

#: List of all the experiments to run
experiment_names = list(snakeconfig["experiments"].keys())

#: List of the main experiments, to run for non-training related plots
main_experiment_names = str_to_list(snakeconfig["main_experiment_names"])

#: List of the test dataset names to use for plots that do not depend on an experiment
standalone_test_dataset_names = str_to_list(
    snakeconfig["standatalone_test_dataset_names"]
)

#: List of the detectors for the rules with only test data
detectors = str_to_list(snakeconfig["detectors"])

#: Associates a step with the experiment names to run for this step
step_to_experiment_names = {
    step: [
        experiment_name
        for experiment_name, experiment_config in snakeconfig["experiments"].items()
        if experiment_config["steps"] is None or step in experiment_config["steps"]
    ]
    for step in STEPS
}

#: Associates an experiment with the test dataset names used to choose the pipeline
#: parameters
experiment_name_to_choice_test_dataset_names = {
    experiment_name: str_to_list(experiment_config["choice"])
    for experiment_name, experiment_config in snakeconfig["experiments"].items()
}


def get_test_csv_sample_paths(
    test_dataset_name: str, detector: str
) -> typing.List[str]:
    """Get a list of paths to the ``.parquet.lz4`` files of a test test dataset."""
    return [
        op.join(
            cdirs.reference_directory,
            f"{test_dataset_name}",
            "xdigi2csv",
            parquet_filename,
        )
        for parquet_filename in DETECTOR_TO_PARQUET_FILE_NAMES[detector]
    ]
