import os.path as op
from utils.commonutils.ctests import get_available_test_dataset_names
from utils.commonutils.config import cdirs


rule preprocess_test_sample:
    input:
        script="scripts/preprocess_test_sample.py",
        parquets=lambda wildcards: snakeconfig.get_test_csv_sample_paths(
            test_dataset_name=wildcards.test_dataset_name, detector=wildcards.detector
        ),
    output:
        preprocessed_dir=op.join(snakeconfig.preprocessed_test_wdir, "done"),
    shell:
        "python {input.script} "
        "-t {wildcards.test_dataset_name} "
        "-d {wildcards.detector} "


rule evaluate_allen_on_test_sample:
    input:
        script="scripts/evaluation/evaluate_allen_on_test_sample.py",
        preprocessed_dir=op.join(snakeconfig.preprocessed_test_wdir, "done"),
    output:
        report=op.join(
            cdirs.performance_directory,
            "allen",
            "{detector}",
            "{test_dataset_name}",
            "report.txt",
        ),
    params:
        output_dir=lambda wildcards, output: op.dirname(output.report),
    shell:
        "python {input.script} "
        "-t {wildcards.test_dataset_name} "
        "-o {params.output_dir} "
        "-d {wildcards.detector} "


rule test_all:
    input:
        test_config=snakeconfig.TEST_CONFIG_PATH,
        reports=expand(
            rules.evaluate_allen_on_test_sample.output,
            test_dataset_name=[
                "minbias-sim10b-xdigi_v2.4_1496",
                "minbias-sim10b-xdigi_v2.4_1498",
            ],
            detector=snakeconfig.detectors,
        ),
