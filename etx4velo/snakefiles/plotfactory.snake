import os.path as op
from utils.commonutils.config import cdirs
from utils.commonutils.ctests import get_test_batch_dir


rule plot_plane_edge_statistics:
    input:
        script="scripts/plotfactory/plot_plane_edge_statistics.py",
        parquets=lambda wildcards: snakeconfig.get_test_csv_sample_paths(
            test_dataset_name=wildcards.test_dataset_name, detector=wildcards.detector
        ),
    output:
        plot_path=op.join(
            cdirs.analysis_directory,
            "{detector}",
            "hist_plane_diff_{test_dataset_name}.pdf",
        ),
    shell:
        "python {input.script} "
        "-t {wildcards.test_dataset_name} "
        "-o {output.plot_path} "
        "-d {wildcards.detector} "


rule plot_data_statistics:
    input:
        script="scripts/plotfactory/plot_data_statistics.py",
        parquets=lambda wildcards: snakeconfig.get_test_csv_sample_paths(
            test_dataset_name=wildcards.test_dataset_name, detector=wildcards.detector
        ),
    output:
        one_plot_path=op.join(
            cdirs.analysis_directory,
            "{detector}",
            "data_distribution",
            "hist_n_hits_{test_dataset_name}.pdf",
        ),
    params:
        outdir=lambda wildcards, output: op.dirname(output.one_plot_path),
    shell:
        "python {input.script} "
        "-t {wildcards.test_dataset_name} "
        "-o {params.outdir} "
        "-d {wildcards.detector} "


rule plot_embedding_explanation:
    input:
        script="scripts/plotfactory/plot_embedding_explanation.py",
        batch_dir=op.join(
            get_test_batch_dir(
                experiment_name="{experiment_name}",
                stage="embedding_processed",
                test_dataset_name="{test_dataset_name}",
            ),
            "done",
        ),
    output:
        one_plot_path=op.join(
            cdirs.analysis_directory,
            "embedding_explanation",
            "hits_only_{experiment_name}_{test_dataset_name}.pdf",
        ),
    params:
        outdir=lambda wildcards, output: op.dirname(output.one_plot_path),
    shell:
        "python {input.script} "
        "-x {wildcards.experiment_name} "
        "-t {wildcards.test_dataset_name} "
        "-o {params.outdir} "


rule plotfactory_all:
    input:
        expand(
            rules.plot_plane_edge_statistics.output.plot_path,
            test_dataset_name=snakeconfig.standalone_test_dataset_names,
            detector=snakeconfig.detectors,
        ),
        expand(
            rules.plot_data_statistics.output.one_plot_path,
            test_dataset_name=snakeconfig.standalone_test_dataset_names,
            detector=snakeconfig.detectors,
        ),
        expand_experiment_names_test_dataset_names(
            rules.plot_embedding_explanation.output.one_plot_path,
            choice=True,
            main_experiments=True,
        ),
