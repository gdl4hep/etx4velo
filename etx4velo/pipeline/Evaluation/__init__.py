"""This package allow to perform matching and track-finding evaluation
after training the pipeline.
It utilises the MonteTracko library.
"""
