"""A module that defines common configurations for the plots.
"""
import numpy as np
import matplotlib as mpl

#: Associates a partition name with a color
partition_to_color = {
    "train": "blue",
    "val": "purple",
    "test": "green",
}

#: Associates a partition name with a label
partition_to_label = {
    "train": "Training",
    "val": "Validation",
    "test": "Test",
}


#: Associates a column name with its label for the plots
column_labels = {
    "pt": "$p_T$ [MeV/c]",
    "p": "$p$ [MeV/c]",
    "px": "$p_x$ [MeV/c]",
    "py": "$p_y$ [MeV/c]",
    "pz": "$p_z$ [MeV/c]",
    "eta": r"$\eta$",
    "phi": r"$\phi$",
    "vz": r"$v_z$ [mm]",
    "nhits_velo": r"\# hits",
    "n_repeated_planes": r"\# hits in same plane",
    "n_skipped_planes": r"\# skipped planes",
    "r_squared": "$R^2$",
    "distance_to_line": "Distance to line [mm]",
    "distance_to_z_axis": "Distance to the $z$-axis [mm]",
    "xz_angle": "Angle to $x$-$z$ plane [Degree]",
    "yz_angle": "Angle to $y$-$z$ plane [Degree]",
    "n_shared_hits": r"\# shared hits",
    "n_unique_planes": r"\# unique planes",
    "quadratic_coeff": r"Quadratic coefficient",
    "pl": r"$\sqrt{p^2 - p_T^2}$ [MeV/c]",
}

#: Associates a column name with its range for the plots
column_ranges = {
    "pt": (0, 2000),
    # "px": (0, 6000),
    "p": (0, 50000),
    "eta": (2.0, 5.0),
    "vz": (-200, 650),
    "distance_to_line": (0.0, 0.15),
    "distance_to_z_axis": (0.0, 0.10),
    # "quadratic_coeff": (-0.0005, 0.0005)
    # "xz_angle": (0.0, 0.5),
    # "yz_angle": (0.0, 0.5),
}

column_bins = {
    "n_repeated_planes": np.arange(4) - 0.5,
    "n_unique_planes": np.arange(3, 16) - 0.5,
    "n_skipped_planes": np.arange(4) - 0.5,
    "n_shared_hits": np.arange(4) - 0.5,
    "nhits_velo": np.arange(3, 19) - 0.5,
    "xz_angle": np.linspace(0.0, 0.15, 20),
    "yz_angle": np.linspace(0.0, 0.15, 20),
    "quadratic_coeff": np.linspace(0.0, 0.00005, 40),
    "px": np.linspace(0.0, 2000.0, 40),
    "py": np.linspace(0.0, 2000.0, 40),
    "pz": np.linspace(0.0, 50000.0, 40),
}

integer_columns = [
    "n_repeated_planes",
    "n_unique_planes",
    "n_skipped_planes",
    "n_shared_hits",
    "nhits_velo",
]


def configure_matplotlib():
    """Set up ``rcParams`` matplotlib object to configure font, fontsizes, etc."""
    mpl.rcParams.update(
        **{
            # Font
            "font.family": "serif",
            "font.serif": "Computer Modern Roman",
            # "font.serif": "Times",
            # Latex
            "text.latex.preamble": r"\usepackage{amsmath}",
            "text.usetex": False,  # Put to False to disable Latex
            # Fontsizes
            "legend.fontsize": 20,
            "axes.titlesize": 24,
            "xtick.labelsize": 20,
            "ytick.labelsize": 20,
            "axes.labelsize": 25,
            "font.size": 20,
            # Lines
            "lines.markersize": 6.0,
            "lines.linestyle": "-",
            "lines.linewidth": 1.5,
            # Figure
            "figure.figsize": (8, 6),
            # "figure.dpi": 200,
            # Ticks
            # Ticks settings
            "xtick.direction": "in",
            "ytick.direction": "in",
        }
    )
