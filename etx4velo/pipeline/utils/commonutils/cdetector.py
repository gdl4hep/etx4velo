"""A module to handle different detectors.
"""
import typing


def get_coordinate_names(detector: str) -> typing.List[str]:
    """Get the list of coordinates names for the given detector

    Args:
        detector: Detector name

    Returns:
        List of coordinate names
    """
    if detector == "velo":
        return ["x", "y", "z"]
    elif detector == "scifi_xz":
        return ["zatyeq0", "xatyeq0"]
    elif detector == "scifi":
        return ["zatyeq0", "xatyeq0", "dxdy"]
    else:
        raise ValueError(
            f"`detector` should either be `velo` or `scifi`, but is {detector}"
        )
