"""A module that defines common utilities for handling files and directories.
"""
import os
import shutil
import logging


def delete_directory(dir: str):
    """Delete a directory. Does not raise an error if the directory does not exit."""
    if os.path.isdir(dir):
        shutil.rmtree(dir)
        logging.info(f"Remove directory `{dir}`.")


def is_directory_not_empty(dir: str) -> bool:
    """Check whether a directory exists and is not empty.
    """
    return os.path.isdir(dir) and len(os.listdir(dir)) > 0
