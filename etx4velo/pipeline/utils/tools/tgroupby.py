"""A module that defines function to efficiently groupby using numba.
"""
import numpy as np
import numba as nb
from montetracko.array_utils.groupby import group_lengths


@nb.jit(nopython=True, cache=True)
def get_group_indices_from_group_lengths(array_group_lengths: np.ndarray):
    """Get the array of starting indices of each group of values in a sorted array.

    Args:
        counts: Array of length of every group after grouping by

    Returns:
        An array that contains the starting index of each group of values in the
        ``sorted_array``. The last element corresponds to the end index of the
        last group.
    """
    n_unique_values = array_group_lengths.shape[0]
    indices_groupby_particles = np.zeros(
        shape=n_unique_values + 1,
        dtype=array_group_lengths.dtype,
    )
    indices_groupby_particles[1:] = np.cumsum(array_group_lengths)
    return indices_groupby_particles


@nb.jit(nopython=True, cache=True)
def get_group_indices(sorted_array: np.ndarray):
    """Get the array of starting indices of each group of values in a sorted array.

    Args:
        sorted_array: a sorted one-dimensional array

    Returns:
        An array that contains the starting index of each group of values in the
        ``sorted_array``. The last element corresponds to the end index of the
        last group.
    """
    n_elements_per_unique_value, _ = group_lengths(sorted_array)
    return get_group_indices_from_group_lengths(
        array_group_lengths=n_elements_per_unique_value
    )
