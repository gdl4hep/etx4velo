"""A module that defines utilies to handle strings.
"""
from string import Formatter


class MyFormatter(Formatter):
    """Class used for formatting not all the arguments of a string.
    Taken from
    https://stackoverflow.com/questions/17215400/format-string-unused-named-arguments


    Examples:
        >>> fmt=MyFormatter()
        >>> fmt.format("{a}{b}", a="blabla")
        'blabla{b}'
    """

    def __init__(self, default="{{{0}}}"):
        self.default = default

    def get_value(self, key, args, kwargs):
        if isinstance(key, str):
            return kwargs.get(key, self.default.format(key))
        else:
            return Formatter.get_value(key, args, kwargs)


fmt = MyFormatter()


def partial_format(string: str, **kwargs) -> str:
    return fmt.format(string, **kwargs)
