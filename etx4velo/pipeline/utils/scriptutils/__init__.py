"""A package that defines common utilies used in the scripts.
"""
from .parser import parse_args
from .loghandler import configure_logger, headline

__all__ = ["parse_args", "configure_logger", "headline"]
