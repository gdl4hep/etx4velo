"""A module to handle the log.
"""
import logging


def configure_logger(
    level=logging.INFO,
    format="%(levelname)s:%(message)s",
    **kwargs,
):
    """Configure the logger with the basic logging.

    All the arguments are passed to :py:func:`logging.basicConfig`.
    """
    logging.basicConfig(level=level, format=format, **kwargs)


def headline(message):
    buffer_len = (80 - len(message)) // 2 if len(message) < 80 else 0
    return "-" * buffer_len + " " + message + " " + "-" * buffer_len
