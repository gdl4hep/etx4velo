"""A package that handles the embedding stage of the pipeline.
This stage consists in creating a rough graph by embedding the hit coordinates
into an embedding space. The embedding network is trained in such a way
that hits that are likely to be connected by an edge are brought clone to another,
while disconnected hits brought apart.

Then, a :math:`k`-nearest neighbour algorithm is applied to create the rough graph.
"""
