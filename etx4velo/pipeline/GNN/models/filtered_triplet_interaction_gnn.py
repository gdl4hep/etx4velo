import typing

import torch


from utils.modelutils.mlp import make_mlp
from GNN.triplet_gnn_base import TripletGNNBase
from GNN.models.triplet_interaction_gnn import TripletInteractionGNN


class FilteredTripletInteractionGNN(TripletInteractionGNN):
    """A triplet-based interaction network."""

    @property
    def with_intermediate_losses(self) -> bool:
        return self.hparams.get("with_intermediate_losses", False)

    @property
    def n_hiddens(self) -> int:
        return self.hparams["n_hiddens"]

    @property
    def with_reslink(self) -> bool:
        return self.hparams.get("with_reslink", False)

    def __init__(self, hparams):
        super(TripletGNNBase, self).__init__(hparams)
        """
        Initialise the Lightning Module that can scan over different GNN training
        regimes
        """
        output_activation = self.hparams.get("output_activation")
        n_steps = self.hparams["n_graph_iters"]
        n_hiddens = self.hparams["n_hiddens"]

        n_node_hiddens = self.hparams["n_node_hiddens"]
        n_edge_hiddens = self.hparams["n_edge_hiddens"]
        n_edge_classifier_hiddens = self.hparams["n_edge_classifier_hiddens"]
        min_edge_scores = self.hparams["intermediate_edge_score_cuts"]

        list_n_node_layers = (
            nb_node_layers
            if isinstance((nb_node_layers := hparams["nb_node_layers"]), list)
            else [nb_node_layers] * n_steps
        )
        list_n_edge_layers = (
            nb_edge_layers
            if isinstance((nb_edge_layers := hparams["nb_edge_layers"]), list)
            else [nb_edge_layers] * n_steps
        )

        # Setup input network
        self.node_encoder = make_mlp(
            self.get_n_features(),
            (
                [self.hparams["n_node_encoding_hiddens"]]
                * (hparams["nb_node_encoder_layers"] - 1)
                + [n_hiddens]
            ),
            output_activation=output_activation,
            hidden_activation=hparams["hidden_activation"],
            layer_norm=hparams["layernorm"],
        )

        # The edge network computes new edge features from connected nodes
        self.edge_encoder = make_mlp(
            2 * (n_hiddens),
            (
                [self.hparams["n_edge_encoding_hiddens"]]
                * (hparams["nb_edge_encoder_layers"] - 1)
                + [n_hiddens]
            ),
            layer_norm=hparams["layernorm"],
            output_activation=output_activation,
            hidden_activation=hparams["hidden_activation"],
        )

        # The edge network computes new edge features from connected nodes
        self.edge_networks = torch.nn.ModuleList(
            [
                make_mlp(
                    3 * n_hiddens,
                    (
                        [n_node_hiddens[step]] * (list_n_edge_layers[step] - 1)
                        + [n_hiddens]
                    ),
                    layer_norm=hparams["layernorm"],
                    output_activation=output_activation,
                    hidden_activation=hparams["hidden_activation"],
                )
                for step in range(n_steps)
            ]
        )

        # The node network computes new node features
        self.node_networks = torch.nn.ModuleList(
            [
                make_mlp(
                    3 * n_hiddens,
                    (
                        [n_edge_hiddens[step]] * (list_n_node_layers[step] - 1)
                        + [n_hiddens]
                    ),
                    layer_norm=hparams["layernorm"],
                    output_activation=output_activation,
                    hidden_activation=hparams["hidden_activation"],
                )
                for step in range(n_steps)
            ]
        )

        # Final edge output classificatioin network
        self.output_edge_classifiers = torch.nn.ModuleList(
            [
                (
                    make_mlp(
                        n_hiddens,
                        (
                            [n_edge_classifier_hiddens[step]]
                            * hparams["nb_edge_classifier_layers"]
                            + [1]
                        ),
                        layer_norm=hparams["layernorm"],
                        output_activation=None,
                        hidden_activation=hparams["hidden_activation"],
                    )
                    if (step == n_steps - 1) or (min_edge_scores[step] is not None)
                    else torch.nn.Identity()
                )
                for step in range(n_steps)
            ]
        )
        self.output_triplet_classifier = make_mlp(
            2 * n_hiddens,
            (
                [self.hparams["n_triplet_classifier_hiddens"]]
                * hparams["nb_edge_classifier_layers"]
                + [1]
            ),
            layer_norm=hparams["layernorm"],
            output_activation=None,
            hidden_activation=hparams["hidden_activation"],
        )

    @property
    def intermediate_edge_score_cuts(self) -> typing.List[int]:
        return self.hparams["intermediate_edge_score_cuts"]

    def output_step(
        self,
        h: torch.Tensor,
        start: torch.Tensor,
        end: torch.Tensor,
        e: torch.Tensor,
        step: int,
    ) -> torch.Tensor:
        """Apply the edge output classifier to edges to get edge logits."""
        assert not isinstance(self.output_edge_classifiers[step], torch.nn.Identity)
        return self.output_edge_classifiers[step](e).squeeze(-1)

    def message_step(
        self,
        h: torch.Tensor,
        start: torch.Tensor,
        end: torch.Tensor,
        e: torch.Tensor,
        step: int,
    ) -> typing.Tuple[torch.Tensor, torch.Tensor]:
        """Apply one step of message-passing that updates the node and edge
        encodings.
        """
        node_inputs = torch.cat(
            (
                h,
                self.scatter_add(e, end, h=h),
                self.scatter_add(e, start, h=h),
            ),
            dim=-1,
        )

        h_new = self.node_networks[step](node_inputs)
        if self.with_reslink:
            h_new = h_new + h

        # Compute new edge features
        edge_inputs = torch.cat([h_new[start], h_new[end], e], dim=-1)
        e_new = self.edge_networks[step](edge_inputs)
        if self.with_reslink:
            e_new = e_new + e
        return h_new, e_new

    def forward_edges(
        self,
        x: torch.Tensor,
        start: torch.Tensor,
        end: torch.Tensor,
    ) -> typing.Dict[str, torch.Tensor]:
        """Forward step for edge classification.

        Args:
            x: Hit features
            edge_index: Torch tensor with 2 rows that define the edges.

        Returns:
            A tuple of 3 tensors: the hit encodings and edge encodings after message
            passing, and the edge classifier output.
        """
        # Encode the graph features into the hidden space
        # Encode the graph features into the hidden space
        h = self.node_encoder(x)
        e = self.edge_encoder(torch.cat((h[start], h[end]), dim=-1))

        # Loop over iterations of edge and node networks
        n_edges = start.shape[0]
        remaining_edge_indices = torch.arange(
            n_edges, dtype=torch.int64, device=self.device
        )
        overall_edge_output = torch.zeros(n_edges, dtype=x.dtype, device=self.device)
        n_graph_iters = self.hparams["n_graph_iters"]
        intermediate_edge_score_cuts = self.intermediate_edge_score_cuts

        intermediate_outputs: typing.List[torch.Tensor] | None = (
            [] if self.with_intermediate_losses else None
        )
        intermediate_indices: typing.List[torch.Tensor] | None = (
            [] if self.with_intermediate_losses else None
        )

        for step in range(n_graph_iters):
            h, e = self.message_step(h, start, end, e, step)

            if (step == n_graph_iters - 1) or (
                (min_edge_score := intermediate_edge_score_cuts[step]) is not None
            ):
                edge_output = self.output_step(h, start, end, e, step=step)
                overall_edge_output = overall_edge_output.clone()
                overall_edge_output[remaining_edge_indices] = edge_output

                if intermediate_outputs is not None:
                    intermediate_outputs.append(edge_output)
                    assert intermediate_indices is not None
                    intermediate_indices.append(remaining_edge_indices)

                if step < n_graph_iters - 1:  # not the last step
                    # Filter the edges
                    assert min_edge_score is not None
                    edge_score = torch.sigmoid(edge_output)
                    edge_mask = edge_score > min_edge_score
                    remaining_edge_indices = remaining_edge_indices[edge_mask]
                    start = start[edge_mask]
                    end = end[edge_mask]
                    e = e[edge_mask]

        overall_e = torch.zeros(size=(n_edges, e.shape[1]), device=e.device)
        overall_e[remaining_edge_indices, :] = e

        outputs = {
            "h": h,
            "e": overall_e,
            "edge_output": overall_edge_output,
        }
        if intermediate_outputs is not None:
            outputs["extended_edge_outputs"] = intermediate_outputs
            assert intermediate_indices is not None
            outputs["extended_indices"] = intermediate_indices
        return outputs

    def _onnx_edge(
        self, x: torch.Tensor, start: torch.Tensor, end: torch.Tensor
    ) -> torch.Tensor:
        """Forward pass for the ``edge`` subnetwork."""
        # Encode the graph features into the hidden space
        h = self.node_encoder(x)
        e = self.edge_encoder(torch.cat((h[start], h[end]), dim=-1))

        # Loop over iterations of edge and node networks
        n_edges = start.shape[0]
        # overall_edge_mask = torch.ones(n_edges, dtype=torch.bool, device=self.device)
        overall_edge_score = torch.zeros(n_edges, dtype=x.dtype, device=self.device)
        overall_edge_mask = torch.ones(n_edges, dtype=torch.bool, device=self.device)
        n_graph_iters = self.hparams["n_graph_iters"]
        intermediate_edge_score_cuts = self.intermediate_edge_score_cuts
        for step in range(n_graph_iters):
            h, e = self.message_step(h, start, end, e, step)
            edge_score = torch.sigmoid(self.output_step(h, start, end, e, step=step))
            overall_edge_score[overall_edge_mask] = edge_score

            if step < n_graph_iters - 1:
                edge_mask = edge_score > intermediate_edge_score_cuts[step]
                start = start[edge_mask]
                end = end[edge_mask]
                e = e[edge_mask]
                overall_edge_mask[overall_edge_mask.clone()] = edge_mask

        return overall_edge_score

    def _onnx_edge_step_0(
        self, x: torch.Tensor, start: torch.Tensor, end: torch.Tensor
    ):
        h = self.node_encoder(x)
        e = self.edge_encoder(torch.cat((h[start], h[end]), dim=-1))

        # Loop over iterations of edge and node networks
        # overall_edge_mask = torch.ones(n_edges, dtype=torch.bool, device=self.device)
        h, e = self.message_step(h, start, end, e, 0)
        edge_score = torch.sigmoid(self.output_step(h, start, end, e, step=0))

        return edge_score

    @property
    def subnetwork_to_outputs(self) -> typing.Dict[str, typing.List[str]]:
        return {
            **super(TripletInteractionGNN, self).subnetwork_to_outputs,
            "edge_step_0": ["edge_score"],
        }
