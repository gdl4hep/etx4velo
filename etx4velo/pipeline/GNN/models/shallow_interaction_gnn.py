import typing

import torch

from .triplet_interaction_gnn import TripletInteractionGNN
from ..triplet_gnn_base import TripletGNNBase
from utils.modelutils.mlp import make_mlp


class ShallowInteractionGNN(TripletInteractionGNN):
    @property
    def n_graph_iters(self) -> int:
        return 1

    @property
    def n_node_encoder_units(self) -> typing.List[int]:
        return self.hparams["n_node_encoder_units"]

    @property
    def n_edge_encoder_units(self) -> typing.List[int]:
        return self.hparams["n_edge_encoder_units"]

    @property
    def n_node_network_units(self) -> typing.List[int]:
        return self.hparams["n_node_network_units"]

    @property
    def n_edge_network_units(self) -> typing.List[int]:
        return self.hparams["n_edge_network_units"]

    @property
    def n_edge_classifier_units(self) -> typing.List[int]:
        return self.hparams["n_edge_classifier_units"]

    @property
    def recursive(self) -> bool:
        return False

    @property
    def n_hiddens(self) -> int:
        return 32

    @property
    def with_triplets(self) -> bool:
        return False

    def __init__(self, hparams):
        super(TripletGNNBase, self).__init__(hparams)
        output_activation = self.hparams.get("output_activation")

        self.node_encoder = make_mlp(
            self.get_n_features(),
            self.n_node_encoder_units,
            output_activation=output_activation,
            hidden_activation=hparams["hidden_activation"],
            layer_norm=hparams["layernorm"],
        )
        self.edge_encoder = make_mlp(
            2 * self.n_node_encoder_units[-1],
            self.n_edge_encoder_units,
            output_activation=output_activation,
            hidden_activation=hparams["hidden_activation"],
            layer_norm=hparams["layernorm"],
        )
        self.node_network = make_mlp(
            2 * self.n_edge_encoder_units[-1] + self.n_node_encoder_units[-1],
            self.n_node_encoder_units,
            output_activation=output_activation,
            hidden_activation=hparams["hidden_activation"],
            layer_norm=hparams["layernorm"],
        )
        self.edge_network = make_mlp(
            2 * self.n_node_encoder_units[-1] + self.n_edge_encoder_units[-1],
            self.n_edge_encoder_units,
            output_activation=output_activation,
            hidden_activation=hparams["hidden_activation"],
            layer_norm=hparams["layernorm"],
        )
        self.output_edge_classifier = make_mlp(
            self.n_edge_encoder_units[-1],
            self.n_edge_classifier_units + [1],
            hidden_activation=hparams["hidden_activation"],
            output_activation=None,
            layer_norm=hparams["layernorm"],
        )

    def forward_edges(
        self, x: torch.Tensor, start: torch.Tensor, end: torch.Tensor
    ) -> typing.Dict[str, torch.Tensor]:
        h = self.node_encoder(x)
        e = self.edge_encoder(torch.cat((h[start], h[end]), dim=-1))

        node_inputs = torch.cat(
            (
                h,
                self.scatter_add(e, end, h=h),
                self.scatter_add(e, start, h=h),
            ),
            dim=-1,
        )

        h = self.node_network(node_inputs)
        edge_inputs = torch.cat([h[start], h[end], e], dim=-1)
        e = self.edge_network(edge_inputs)

        return {"edge_output": self.output_edge_classifier(e).squeeze(-1)}
