"""A module that defines a triplet interaction GNN, for the SciFi.
"""
import typing
import torch
from torch.utils.checkpoint import checkpoint
from torch_scatter import scatter_add, scatter_max

from ..triplet_gnn_base import TripletGNNBase, filter_edges_for_triplets
from utils.modelutils.mlp import make_mlp
from utils.tools import tarray


class SciFiTripletInteractionGNN(TripletGNNBase):
    def __init__(self, hparams):
        super().__init__(hparams)
        nb_node_layers: int = hparams["nb_node_layers"]
        nb_edge_layers: int = hparams["nb_edge_layers"]
        nb_hidden: int = hparams["hidden"]

        self.node_encoder = make_mlp(
            2,
            [nb_hidden] * hparams.get("nb_node_encoder_layers", nb_node_layers),
            output_activation=None,
            hidden_activation=hparams["hidden_activation"],
            layer_norm=hparams["layernorm"],
        )

        # Let's just try having 2 superimposed interaction GNN...
        self.uv_node_encoder = make_mlp(
            3,
            [nb_hidden] * hparams.get("nb_node_encoder_layers", nb_node_layers),
            output_activation=None,
            hidden_activation=hparams["hidden_activation"],
            layer_norm=hparams["layernorm"],
        )

        # The edge network computes new edge features from connected nodes
        self.ux_vx_edge_encoder = make_mlp(
            2 * (nb_hidden),
            [nb_hidden] * hparams.get("nb_edge_encoder_layers", nb_edge_layers),
            layer_norm=hparams["layernorm"],
            output_activation=None,
            hidden_activation=hparams["hidden_activation"],
        )

        # The edge network computes new edge features from connected nodes
        self.ux_vx_edge_network = make_mlp(
            3 * nb_hidden,
            [nb_hidden] * nb_edge_layers,
            layer_norm=hparams["layernorm"],
            output_activation=None,
            hidden_activation=hparams["hidden_activation"],
        )

        self.node_network = make_mlp(
            7 * nb_hidden,
            [nb_hidden] * nb_node_layers,
            layer_norm=hparams["layernorm"],
            output_activation=None,
            hidden_activation=hparams["hidden_activation"],
        )

        # The node network computes new node features
        self.uv_node_network = make_mlp(
            3 * nb_hidden,
            [nb_hidden] * nb_node_layers,
            layer_norm=hparams["layernorm"],
            output_activation=None,
            hidden_activation=hparams["hidden_activation"],
        )

        # Final edge output classification network
        self.ux_vx_output_edge_classifier = make_mlp(
            3 * nb_hidden,
            [nb_hidden] * hparams.get("nb_edge_classifier_layers", nb_edge_layers)
            + [1],
            layer_norm=hparams["layernorm"],
            output_activation=None,
            hidden_activation=hparams["hidden_activation"],
        )

    def message_step(
        self,
        x,
        uv,
        start_x,
        end_x,
        start_ux_vx,
        end_ux_vx,
        e_x,
        e_ux_vx,
    ):
        edge_messages_x = torch.cat(
            [
                scatter_max(e_x, end_x, dim=0, dim_size=x.shape[0])[0],
                scatter_add(e_x, end_x, dim=0, dim_size=x.shape[0]),
                scatter_max(e_x, start_x, dim=0, dim_size=x.shape[0])[0],
                scatter_add(e_x, start_x, dim=0, dim_size=x.shape[0]),
                scatter_max(e_ux_vx, end_ux_vx, dim=0, dim_size=x.shape[0])[0],
                scatter_add(e_ux_vx, end_ux_vx, dim=0, dim_size=x.shape[0]),
            ],
            dim=-1,
        )  # type: ignore
        x_out = self.node_network(torch.cat((x, edge_messages_x), dim=-1)) + x

        edge_messages_uv = torch.cat(
            [
                scatter_max(e_ux_vx, start_ux_vx, dim=0, dim_size=uv.shape[0])[0],
                scatter_add(e_ux_vx, start_ux_vx, dim=0, dim_size=uv.shape[0]),
            ],
            dim=-1,
        )  # type: ignore
        uv_out = self.uv_node_network(torch.cat((uv, edge_messages_uv), dim=-1)) + uv

        # Compute new edge features
        e_x_out = (
            self.edge_network(torch.cat((x_out[start_x], x_out[end_x], e_x), dim=-1))
            + e_x
        )
        e_ux_vx_out = (
            self.ux_vx_edge_network(
                torch.cat((uv_out[start_ux_vx], x_out[end_ux_vx], e_ux_vx), dim=-1)
            )
            + e_ux_vx
        )

        return x_out, uv_out, e_x_out, e_ux_vx_out

    def split_xuv(self, features: torch.Tensor, edge_index: torch.Tensor):
        is_x = features[:, -1] == 0.0
        is_uv = ~is_x
        x = features[is_x, :2]
        uv = features[is_uv]

        old_to_new_indices = torch.zeros(
            features.shape[0],
            dtype=edge_index.dtype,
            device=edge_index.device,
        )
        old_to_new_indices[is_x] = torch.arange(
            end=is_x.sum(), dtype=edge_index.dtype, device=edge_index.device  # type: ignore
        )
        old_to_new_indices[is_uv] = torch.arange(
            end=(is_uv).sum(), dtype=edge_index.dtype, device=edge_index.device  # type: ignore
        )

        edge_index_has_u = is_uv[edge_index[1]]
        edge_index_has_v = is_uv[edge_index[0]]

        edge_index_x = edge_index[:, (~edge_index_has_u) & (~edge_index_has_v)]
        edge_index_xu = edge_index[:, edge_index_has_u]
        edge_index_vx = edge_index[:, edge_index_has_v]

        assert not torch.any(edge_index_has_u & edge_index_has_v)

        # edge_index_uv = edge_index[:, (edge_index_has_u) & (edge_index_has_v)]
        # print(edge_index_uv)
        # assert torch.all((x[edge_index_x] == 0.0))

        edge_index_ux_vx = torch.cat(
            (edge_index_xu.flip(0), edge_index_vx),
            dim=-1,
        )
        return (
            x,
            uv,
            old_to_new_indices[edge_index_x],
            old_to_new_indices[edge_index_ux_vx],
            edge_index_has_u,
            edge_index_has_v,
        )

    def forward_edges(
        self,
        x: torch.Tensor,  # type: ignore
        uv: torch.Tensor,  # type: ignore
        edge_index_x: torch.Tensor,
        edge_index_ux_vx: torch.Tensor,
    ) -> typing.Tuple[torch.Tensor, torch.Tensor]:
        x.requires_grad = True
        uv.requires_grad = True

        start_x, end_x = edge_index_x
        start_ux_vx, end_ux_vx = edge_index_ux_vx
        # Encode the graph features into the hidden space
        # Node encoding
        x: torch.Tensor = self.node_encoder(x)  # type: ignore
        uv: torch.Tensor = self.uv_node_encoder(uv)  # type: ignore
        # Edge encoding
        e_x: torch.Tensor = self.edge_encoder(torch.cat((x[start_x], x[end_x]), dim=1))  # type: ignore
        e_ux_vx: torch.Tensor = self.ux_vx_edge_encoder(
            torch.cat((uv[start_ux_vx], x[end_ux_vx]), dim=1)
        )

        for _ in range(self.hparams["n_graph_iters"]):
            x, uv, e_x, e_ux_vx = self.message_step(
                x,
                uv,
                start_x,
                end_x,
                start_ux_vx,
                end_ux_vx,
                e_x,
                e_ux_vx,
            )

        # Compute final edge scores; use original edge directions only
        edge_output_x: torch.Tensor = self.output_step(x, start_x, end_x, e_x)  # type: ignore
        edge_output_ux_vx: torch.Tensor = self.output_step_ux_vx(
            x, uv, start_ux_vx, end_ux_vx, e_ux_vx
        )  # type: ignore

        return edge_output_x, edge_output_ux_vx

    def output_step_ux_vx(self, x, uv, start_ux_vx, end_ux_vx, e_ux_vx):
        classifier_inputs = torch.cat((uv[start_ux_vx], x[end_ux_vx], e_ux_vx), dim=1)
        return self.output_edge_classifier(classifier_inputs).squeeze(-1)
        # return self.ux_vx_output_edge_classifier(classifier_inputs).squeeze(-1)

    def forward(
        self,
        x: torch.Tensor,
        edge_index: torch.Tensor,
        df_edges: tarray.DataFrame,
        edge_score_cut: float | None = None,
        with_triplets: bool = True,
    ) -> typing.Dict[str, typing.Any]:
        (
            x,
            uv,
            edge_index_x,
            edge_index_ux_vx,
            edge_index_has_u,
            edge_index_has_v,
        ) = self.split_xuv(features=x, edge_index=edge_index)

        edge_output_x, edge_output_ux_vx = checkpoint(  # type: ignore
            self.forward_edges,
            x=x,
            uv=uv,
            edge_index_x=edge_index_x,
            edge_index_ux_vx=edge_index_ux_vx,
            use_reentrant=True,
        )
        edge_output_overall = torch.zeros(
            size=(edge_index.shape[1],),
            dtype=edge_output_ux_vx.dtype,
            device=edge_output_ux_vx.device,
        )
        edge_output_overall[(~edge_index_has_u) & (~edge_index_has_v)] = edge_output_x
        edge_output_overall[edge_index_has_u] = edge_output_ux_vx[
            : edge_index_has_u.sum()
        ]
        edge_output_overall[edge_index_has_v] = edge_output_ux_vx[
            edge_index_has_u.sum() :
        ]

        edge_score_cut_ = (
            self.hparams["edge_score_cut"] if edge_score_cut is None else edge_score_cut
        )
        df_edges, filtered_edge_index, old_edge_indices = filter_edges_for_triplets(
            df_edges=df_edges,
            edge_mask=torch.sigmoid(edge_output_overall) > edge_score_cut_,
        )

        outputs = {}
        outputs["edge_output"] = edge_output_overall
        outputs["filtered_edge_index"] = filtered_edge_index
        outputs["filtered_edge_score"] = torch.sigmoid(edge_output_overall)[
            old_edge_indices
        ]
        outputs["edge_partitions"] = {
            "x": (~edge_index_has_u) & (~edge_index_has_v),
            "xu": edge_index_has_u,
            "vx": edge_index_has_u,
        }

        if with_triplets:
            raise NotImplementedError()

        return outputs
