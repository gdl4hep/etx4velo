import typing

import torch
from GNN.models.triplet_interaction_gnn import TripletInteractionGNN


class SimpleFilteredTripletInteractionGNN(TripletInteractionGNN):
    """A triplet-based interaction network."""

    @property
    def intermediate_edge_score_cuts(self) -> typing.List[int]:
        return self.hparams["intermediate_edge_score_cuts"]

    def forward_edges(
        self, x: torch.Tensor, start: torch.Tensor, end: torch.Tensor
    ) -> typing.Dict[str, torch.Tensor]:
        """Forward step for edge classification.

        Args:
            x: Hit features
            edge_index: Torch tensor with 2 rows that define the edges.

        Returns:
            A tuple of 3 tensors: the hit encodings and edge encodings after message
            passing, and the edge classifier output.
        """
        # Encode the graph features into the hidden space
        h = self.node_encoder(x)
        e = self.edge_encoder(torch.cat((h[start], h[end]), dim=-1))

        # Loop over iterations of edge and node networks
        n_edges = start.shape[0]
        edge_mask = torch.ones(n_edges, dtype=torch.bool, device=self.device)
        edge_output = torch.zeros(n_edges, dtype=x.dtype, device=self.device)
        n_graph_iters = self.hparams["n_graph_iters"]
        intermediate_edge_score_cuts = self.intermediate_edge_score_cuts
        for step in range(n_graph_iters):
            start_mask = start[edge_mask]
            end_mask = end[edge_mask]
            e_mask = e[edge_mask]

            h, e_mask = self.message_step(h, start_mask, end_mask, e_mask, step)
            edge_output_mask = self.output_step(h, start_mask, end_mask, e_mask)

            e = e.clone()
            e[edge_mask] = e_mask

            edge_output = edge_output.clone()
            edge_output[edge_mask] = edge_output_mask

            if step < n_graph_iters - 1:
                updated_edge_mask = edge_mask.clone()
                updated_edge_mask[edge_mask] = (
                    torch.sigmoid(edge_output_mask) > intermediate_edge_score_cuts[step]
                )
                edge_mask = updated_edge_mask

        return {"h": h, "e": e, "edge_output": edge_output}

    # def _onnx_edge(
    #     self, x: torch.Tensor, start: torch.Tensor, end: torch.Tensor
    # ) -> torch.Tensor:
    #     """Forward pass for the ``edge`` subnetwork."""
    #     # Encode the graph features into the hidden space
    #     h = self.node_encoder(x)
    #     e = self.edge_encoder(torch.cat((h[start], h[end]), dim=-1))

    #     # Loop over iterations of edge and node networks
    #     n_edges = start.shape[0]
    #     # overall_edge_mask = torch.ones(n_edges, dtype=torch.bool, device=self.device)
    #     remaining_edge_indices = torch.arange(
    #         n_edges, dtype=torch.int64, device=self.device
    #     )
    #     overall_edge_score = torch.zeros(n_edges, dtype=x.dtype, device=self.device)
    #     n_graph_iters = self.hparams["n_graph_iters"]
    #     intermediate_edge_score_cuts = self.intermediate_edge_score_cuts
    #     for step in range(n_graph_iters):
    #         h, e = self.message_step(h, start, end, e, step)
    #         edge_score = torch.sigmoid(self.output_step(h, start, end, e))
    #         overall_edge_score[remaining_edge_indices, :] = edge_score

    #         if step < n_graph_iters - 1:
    #             edge_mask = edge_score > intermediate_edge_score_cuts[step]
    #             remaining_edge_indices = remaining_edge_indices[edge_mask]
    #             start = start[edge_mask]
    #             end = end[edge_mask]
    #             e = e[edge_mask]

    #     return overall_edge_score

    def _onnx_edge(
        self, x: torch.Tensor, start: torch.Tensor, end: torch.Tensor
    ) -> torch.Tensor:
        """Forward pass for the ``edge`` subnetwork."""
        # Encode the graph features into the hidden space
        h = self.node_encoder(x)
        e = self.edge_encoder(torch.cat((h[start], h[end]), dim=-1))

        # Loop over iterations of edge and node networks
        n_edges = start.shape[0]
        # overall_edge_mask = torch.ones(n_edges, dtype=torch.bool, device=self.device)
        overall_edge_score = torch.zeros(n_edges, dtype=x.dtype, device=self.device)
        overall_edge_mask = torch.ones(n_edges, dtype=torch.bool, device=self.device)
        n_graph_iters = self.hparams["n_graph_iters"]
        intermediate_edge_score_cuts = self.intermediate_edge_score_cuts
        for step in range(n_graph_iters):
            h, e = self.message_step(h, start, end, e, step)
            edge_score = torch.sigmoid(self.output_step(h, start, end, e))
            overall_edge_score[overall_edge_mask] = edge_score

            if step < n_graph_iters - 1:
                edge_mask = edge_score > intermediate_edge_score_cuts[step]
                start = start[edge_mask]
                end = end[edge_mask]
                e = e[edge_mask]
                overall_edge_mask[overall_edge_mask.clone()] = edge_mask

        return overall_edge_score
