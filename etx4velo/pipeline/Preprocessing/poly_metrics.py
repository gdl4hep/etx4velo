"""Module that implements the fitting of a trajectory to a 2D polynomial.

Adapted from from https://gist.github.com/kadereub/9eae9cff356bb62cdbd672931e8e5ec4
"""
import numpy as np
import numpy.typing as npt
import numba as nb


@nb.jit(nopython=True, cache=True)
def _coeff_mat(x, deg):
    mat_ = np.zeros(shape=(x.shape[0], deg + 1))
    const = np.ones_like(x)
    mat_[:, 0] = const
    mat_[:, 1] = x
    if deg > 1:
        for n in range(2, deg + 1):
            mat_[:, n] = x**n
    return mat_


@nb.jit(nopython=True, cache=True)
def _fit_x(a, b):
    # linalg solves ax = b
    det_ = np.linalg.lstsq(a, b)[0]
    return det_


@nb.jit(nopython=True, cache=True)
def fit_poly(x, y, deg):
    a = _coeff_mat(x, deg)
    p = _fit_x(a, y)
    # Reverse order so p[0] is coefficient of highest order
    return p[::-1]


@nb.jit(nopython=True, cache=True)
def eval_polynomial(P, x):
    """
    Compute polynomial P(x) where P is a vector of coefficients, highest
    order coefficient at P[0].  Uses Horner's Method.
    """
    result = np.zeros_like(x)
    for coeff in P:
        result = x * result + coeff
    return result


@nb.jit(nopython=True, cache=True)
def compute_distance_to_poly(coords: npt.NDArray, coeffs: npt.NDArray) -> float:
    n_points = coords.shape[0]
    if n_points <= 3:
        return 0.0
    else:
        x = coords[:, 0]
        y = coords[:, 1]
        fitted_y = eval_polynomial(P=coeffs, x=x)
        return np.sqrt(np.sum(np.square(fitted_y - y))) / (n_points - 3)


@nb.jit(nopython=True, cache=True)
def compute_quadratic_coeff(coords: npt.NDArray, coeffs: npt.NDArray) -> float:
    n_points = coords.shape[0]
    if n_points <= 2:
        return 0.0
    else:
        return coeffs[0]
