import os
import pandas as pd
import shutil

# Define the input directory
input_dir = "/scratch/fgias/data/__test__/velo/minbias-sim10b-xdigi_v2.4_all"
base_output_dir = "/scratch/fgias/data/__test__/velo/minbias-sim10b-xdigi_v2.4_all_threshold"

# Define the range and increment for thresholds
increment = 500
thresholds = range(increment, 5001, increment)

# Loop over each threshold
for threshold in thresholds:
    # Create the output directory name based on the threshold
    output_dir = f"{base_output_dir}_{threshold}"
    
    # Ensure the output directory exists
    os.makedirs(output_dir, exist_ok=True)
    
    # Initialize the counter for copied files
    copied_files_count = 0
    
    # Loop through all files in the input directory
    for filename in os.listdir(input_dir):
        # Process only the hit files
        if filename.endswith("-hits_particles.parquet"):
            file_path = os.path.join(input_dir, filename)
            
            # Read the parquet file to check the number of hits
            df = pd.read_parquet(file_path)
            
            # Check if the number of hits is within the current threshold range
            if threshold - increment < len(df) < threshold:
                # Determine the base event filename
                base_filename = filename.replace("-hits_particles.parquet", "")
                
                # Define the filenames for the hits and particles files
                hits_file = f"{base_filename}-hits_particles.parquet"
                particles_file = f"{base_filename}-particles.parquet"
                
                # Define the full paths for the source files
                src_hits_path = os.path.join(input_dir, hits_file)
                src_particles_path = os.path.join(input_dir, particles_file)
                
                # Define the full paths for the destination files
                dest_hits_path = os.path.join(output_dir, hits_file)
                dest_particles_path = os.path.join(output_dir, particles_file)
                
                # Copy the files to the output directory
                shutil.copy(src_hits_path, dest_hits_path)
                shutil.copy(src_particles_path, dest_particles_path)
                
                # Increment the counter for copied files
                copied_files_count += 1
                
                print(f"Copied files for event {base_filename} to the directory {output_dir}")
    
    # Print the count of copied files for the current threshold
    print(f"Threshold {threshold}: Copied {copied_files_count} events.")

print("Processing complete.")
