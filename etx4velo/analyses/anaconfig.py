"""Common configurations for the folder analyses.
"""
import os
import os.path as op
import sys

repo_dir = op.abspath(op.join(op.dirname(__file__), "..", ".."))
os.environ["ETX4VELO_REPO"] = repo_dir
sys.path.insert(0, op.join(repo_dir, "etx4velo", "pipeline"))
sys.path.insert(0, op.join(repo_dir, "etx4velo"))
sys.path.insert(0, op.join(repo_dir, "montetracko"))

#: Directory where to save the plots
PLOTDIR = op.abspath(op.join("..", "output", "preliminary"))

#: Directory where the dataframes that are used for the analysis are located
DATAFRAME_DIR = "/scratch/acorreia/minbias-sim10b-xdigi-nospillover/92"
