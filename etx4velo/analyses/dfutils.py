"""A module that defines utilies to convert tensor objects to Pandas dataframes.
"""

import numpy as np
import numpy.typing as npt
import pandas as pd


def get_df_edges(
    edge_indices: npt.ArrayLike,
    df_hits: pd.DataFrame,
) -> pd.DataFrame:
    edge_indices = np.asarray(edge_indices)
    df_edges = pd.DataFrame(
        {
            "hit_idx_left": edge_indices[0],
            "hit_idx_right": edge_indices[1],
        },
    )

    # Add particle ID information
    for side in ["left", "right"]:
        df_edges = df_edges.merge(
            df_hits.rename(  # type: ignore
                columns={column: f"{column}_{side}" for column in df_hits.columns}
            ),
            on=f"hit_idx_{side}",
            how="left",
        )

    return df_edges
