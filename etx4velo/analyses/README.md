# Analyses

This folder contains a collection of scripts and notebooks to answer
typical questions about the data distribution.

How many hits are there in average / event? In average, how many repeated planes
do a track have? This folder contains scripts and notebooks to answer these kind of
questions in a reproducible way.
