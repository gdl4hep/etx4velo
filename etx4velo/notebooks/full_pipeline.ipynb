{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**NB: This notebook is based on the original quick start [Exa.TrkX notebook](https://github.com/HSF-reco-and-software-triggers/Tracking-ML-Exa.TrkX/blob/master/Examples/TrackML_Quickstart/run_quickstart.ipynb)**."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 0. Setup"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Imports"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "%load_ext autoreload\n",
    "%autoreload 2\n",
    "\n",
    "import os\n",
    "import warnings\n",
    "\n",
    "import montetracko.lhcb as mtb\n",
    "\n",
    "from Preprocessing.run_preprocessing import run_preprocessing_test_dataset\n",
    "from Preprocessing.run_preprocessing import run_preprocessing\n",
    "from Processing.run_processing import run_processing_from_config\n",
    "\n",
    "from Embedding.embedding_plots import plot_best_performances_squared_distance_max\n",
    "\n",
    "from scripts.train_model import train_model\n",
    "from scripts.build_graph_using_embedding import run as run_embedding_inference\n",
    "from scripts.build_tracks import build as build_track_candidates\n",
    "\n",
    "from utils.plotutils import performance_mpl as perfplot_mpl\n",
    "from utils.commonutils.ctests import get_required_test_dataset_names\n",
    "from utils.commonutils.config import load_config\n",
    "from utils.modelutils import checkpoint_utils\n",
    "from utils.plotutils.plotconfig import configure_matplotlib\n",
    "from pipeline import load_trained_model\n",
    "\n",
    "# Configure matplotlib plot styles\n",
    "configure_matplotlib()\n",
    "\n",
    "# Disable some warnings\n",
    "warnings.filterwarnings(\n",
    "    \"ignore\", message=(\n",
    "        \"TypedStorage is deprecated. It will be removed in the future and \"\n",
    "        \"UntypedStorage will be the only storage class. This should only matter to you \"\n",
    "        \"if you are using storages directly.\"\n",
    "    )\n",
    ")\n",
    "\n",
    "warnings.filterwarnings(\n",
    "    \"ignore\", \"None of the inputs have requires_grad=True. Gradients will be None\"\n",
    ")\n",
    "\n",
    "# \n",
    "CONFIG = '../pipeline_configs/example.yaml'\n",
    "\n",
    "run_training: bool = True\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 1. Preprocessing\n",
    "\n",
    "The preprocessing consists of reading the initial `hits_velo.parquet.lz4` and `mc_particles.parquet.lz4`\n",
    "files, computing the necessary columns and save the preprocessed files\n",
    "with 2 files `event{run_number}{event_number}-hits_particles.parquet` and\n",
    "`event{run_number}{event_number}-particles.parquet`.\n",
    "\n",
    "These files will be used for the next steps as well as for the final track-finding evaluation."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Preprocess the training and validation datasets\n",
    "\n",
    "For instance, in the `example.yaml` pipeline configuration, the following\n",
    "`processing` have been required: `remove_curved_particles`.\n",
    "This processing corresponds to a function defined in `pipeline/Preprocessing/process_custom.py`,\n",
    "that computes the normalised distance between the hits of each particle and\n",
    "a line fitted to this hits, and apply a requirement to remove particles\n",
    "that are not straight enough.\n",
    "\n",
    "The preprocessed files are saved in `{data_directory}/preprocessed/`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "run_preprocessing(CONFIG, reproduce=False)\n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Preprocess the test datasets"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The preprocessed test samples are common between different experiments.\n",
    "There are saved under `{data_directory}/__test__/{test_dataset_name}`.\n",
    "\n",
    "Note that the processing `remove_curved_particles` is not applied to the test\n",
    "dataset because the test sample needs to be a proxy for real case inference.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for required_test_dataset_name in get_required_test_dataset_names(CONFIG):\n",
    "    run_preprocessing_test_dataset(\n",
    "        test_dataset_name=required_test_dataset_name,\n",
    "        reproduce=False,\n",
    "    )\n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 2. Processing\n",
    "\n",
    "The processing consists in reading the preprocessed files\n",
    "and transform them in Pytorch Gemetric data objects\n",
    "that are saved in pickle files in `{data_directory}/{experiment_name}/processed`.\n",
    "Moreover, the processed samples are splitted into\n",
    "a training set (here of 5,000 events) and a validation set\n",
    "(here, of 500 events).\n",
    "\n",
    "The genuine edges are computed. As configured in the pipeline\n",
    "configuration file, for a given particle, the true edges\n",
    "are hits belonging to the same particles, in increasing plane number.\n",
    "\n",
    "If you do not plan in performing track-finding evaluation on the train and\n",
    "validation samples, you could remove the preprocessed files generated earlier."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "run_processing_from_config(CONFIG, reproduce=False)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The test samples are also processed and stored under `{data_directory}/{experiment_name}/test/{test_dataset_name}`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "for required_test_dataset_name in get_required_test_dataset_names(CONFIG):\n",
    "    run_processing_from_config(\n",
    "        test_dataset_name=required_test_dataset_name,\n",
    "        path_or_config=CONFIG,\n",
    "        reproduce=False,\n",
    "    )\n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `batch` object below corresponds to a single processed event, stored in PyTorch Geometric data object.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "from Embedding.embedding_base import get_example_data\n",
    "_, batch = get_example_data(CONFIG)\n",
    "batch\n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 3. Embedding Network"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {
    "jp-MarkdownHeadingCollapsed": true
   },
   "source": [
    "The initial step of the pipeline involves constructing a preliminary graph\n",
    "aimed at capturing a majority of genuine edges (i.e., high edge efficiency),\n",
    "even though most of the edges will initially be fake (i.e., low edge purity).\n",
    "The subsequent stage, handled by the Graph Neural Network, filters out these fake edges.\n",
    "\n",
    "To create this initial graph, a dense neural network,\n",
    "referred to as the \"Embedding network,\" is applied to the cylindrical coordinates\n",
    "$\\left(r, \\theta, z\\right)$ of each hit.\n",
    "The Embedding network is trained to bring hits likely to be connected by an edge\n",
    "closer to each other and push apart hits expected to be disconnected in the embedding space.\n",
    "\n",
    "In order to built this rough graph, a dense neural network, also called\n",
    "Embedding network is applied on the cylindrical coordinates $\\left(r, \\theta, z\\right)$\n",
    "of each hit.\n",
    "\n",
    "This Embedding network is trained in such a way that hits that are likely\n",
    "to be connected by an edge are brought close to each other, and hits\n",
    "that are likely to be disconnected are brought apart.\n",
    "\n",
    "\n",
    "To achieve this, during training, a sample of both genuine and fake edges is generated.\n",
    "\n",
    "To do so, a sample of genuine and fake edges is built. The embedded distance $d$\n",
    "between the 2 hits of each edge is computed.\n",
    "The embedded distance $d$ between the two hits of each edge is computed.\n",
    "The Embedding network is then trained using a [Hinge Embedding Loss](https://pytorch.org/docs/stable/generated/torch.nn.HingeEmbeddingLoss.html) defined as:\n",
    "$$\n",
    "L = \\text{weight} \\times L_{\\text{positive}} + L_{\\text{negative}}\n",
    "$$\n",
    "\n",
    "where $L_{\\text{positive}} = d^2$ represents the loss for genuine edges,\n",
    "and $L_{\\text{negative}} = \\max{\\left(0, \\text{margin} - d^2\\right)}$\n",
    "represents the loss for fake edges. In this context, $d$ is the distance between\n",
    "the hits of the edge, in the embedding space.\n",
    "This loss function minimises $d$ for genuine edges while maximising $d$ for fake edges.\n",
    "\n",
    "The pipeline configuration file specifies $\\text{weight} = 3$ and $\\text{margin} = 0.010$.\n",
    "\n",
    "Following training, the graph is constructed by applying a $k_{\\text{max}}$-Nearest Neighbour (kNN) algorithm,\n",
    "with the value of $k_{\\text{max}} = 50$ specified in the pipeline configuration.\n",
    "\n",
    "Notably, the resulting graph is unidirectional (indicated as `bidir: False` in the pipeline configuration).\n",
    "This unidirectionality is due to the kNN algorithm being applied for each plane $p$ to find the first\n",
    "$k_{\\text{max}}$ neighbors within the next `plane_range = 2` planes.\n",
    "\n",
    "Subsequently, only edges for which the squared distance between the two hits\n",
    "is below a designated threshold $d^2_{\\text{max}}$ are retained.\n",
    "The choice of $d^2_{\\text{max}}$ is further detailed below."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Train metric learning model\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For this training, the events are loaded in a lazy manner (`lazy: True` in the configuration).\n",
    "n this mode, it becomes feasible to divide a sizable training set (in this case, containing 5,000 events)\n",
    "into smaller sub-epochs.\n",
    "As an example, the training set is partitioned into `trainset_split=10` sub-epochs, each consisting of 500 events.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "if run_training:\n",
    "    embedding_trainer, embedding_model = train_model(\n",
    "        path_or_config=CONFIG, step=\"embedding\"\n",
    "    )"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, we find the path to the artifact file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "embedding_version_dir = checkpoint_utils.get_last_version_dir_from_config(\n",
    "    step=\"embedding\", path_or_config=CONFIG\n",
    ")\n",
    "embedding_metric_path = os.path.join(embedding_version_dir, \"metrics.csv\")\n",
    "embedding_artifact_path = checkpoint_utils.get_last_artifact(\n",
    "    version_dir=embedding_version_dir\n",
    ")\n",
    "print(f\"{embedding_metric_path=}\")\n",
    "print(f\"{embedding_artifact_path=}\")\n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To continue the training of the network, you may use the code below"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# from pytorch_lightning.loggers import CSVLogger\n",
    "# from pytorch_lightning import Trainer\n",
    "\n",
    "\n",
    "# def continue_embedding_training(\n",
    "#     path_or_config: str | dict,\n",
    "# ) -> typing.Tuple[Trainer, LayerlessEmbedding]:\n",
    "#     config = load_config(path_or_config=path_or_config)\n",
    "\n",
    "#     embedding_model = LayerlessEmbedding.load_from_checkpoint(\n",
    "#         embedding_artifact_path\n",
    "#     )  # you may change `embedding_model`\n",
    "\n",
    "#     save_directory = os.path.abspath(\n",
    "#         os.path.join(config[\"common\"][\"artifact_directory\"], \"embedding\")\n",
    "#     )\n",
    "\n",
    "#     logger = CSVLogger(save_directory, name=config[\"common\"][\"experiment_name\"])\n",
    "\n",
    "#     embedding_trainer = Trainer(\n",
    "#         accelerator=\"gpu\" if torch.cuda.is_available() else \"cpu\",\n",
    "#         devices=1,\n",
    "#         max_epochs=40,  # you may increase the number of epochs\n",
    "#         logger=logger,\n",
    "#         # callbacks=[EarlyStopping(monitor=\"train_loss\", mode=\"min\")]\n",
    "#     )\n",
    "\n",
    "#     embedding_trainer.fit(embedding_model)\n",
    "#     return embedding_trainer, embedding_model\n",
    "\n",
    "# # embedding_trainer, embedding_model = continue_embedding_training(CONFIG)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The model is loaded in order to check the performance of the network\n",
    "and choose the optimanl $d^2_{\\text{max}}$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "embedding_model = load_trained_model(path_or_config=CONFIG, step=\"embedding\")\n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Plot training metrics"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can examine how the training went by looking at the loss, edge efficiency\n",
    "and edge purity as a function of the sub-epoch."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# embedding_metrics = checkpoint_utils.get_training_metrics(embedding_trainer) \n",
    "embedding_metrics = checkpoint_utils.get_training_metrics(embedding_metric_path)\n",
    "\n",
    "embedding_metrics"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "perfplot_mpl.plot_loss(metrics=embedding_metrics, path_or_config=CONFIG, step=\"embedding\")\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "perfplot_mpl.plot_metric_epochs(\n",
    "    \"eff\",\n",
    "    embedding_metrics,\n",
    "    \"Edge Efficiency\",\n",
    "    step=\"embedding\",\n",
    "    color=perfplot_mpl.partition_to_color[\"val\"],\n",
    "    path_or_config=CONFIG,\n",
    ")\n",
    "perfplot_mpl.plot_metric_epochs(\n",
    "    \"pur\",\n",
    "    embedding_metrics,\n",
    "    \"Edge Purity\",\n",
    "    step=\"embedding\",\n",
    "    color=perfplot_mpl.partition_to_color[\"val\"],\n",
    "    path_or_config=CONFIG,\n",
    ")\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The training could have been continued."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Selecting the optimal $d^2_\\text{max}$\n",
    "\n",
    "To determine the appropriate maximum squared distance $d^2_{\\text{max}}$ in the embedding\n",
    "space between two hits of an edge, several critical factors are considered\n",
    "including graph edge efficiency, purity, and the number of edges present in the graph.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "from Embedding import embedding_plots\n",
    "\n",
    "embedding_plots.plot_embedding_performance_given_squared_distance_max_k_max(\n",
    "    model=embedding_model,\n",
    "    path_or_config=CONFIG,\n",
    "    squared_distance_max=[0.005, 0.010, 0.015, 0.020, 0.030, 0.040],\n",
    "    n_events=200,\n",
    "    partitions={\"minbias-sim10b-xdigi_v2.4_1498\": None},\n",
    ");\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As $d^2_\\text{max}$ increases, the graph size also grows\n",
    "For very large values of $d^2_\\text{max}$,\n",
    "edge efficiency tends to be high,\n",
    "but an excessive number of edges may negatively impact inference time.\n",
    "\n",
    "To gain a deeper understanding and assist in the selection of an ideal $d^2_\\text{max}$ value,\n",
    "we plot the optimal track-finding performance against different $d^2_\\text{max}$ values.\n",
    "This optimal performance is calculated by removing all fake edges from the graph\n",
    "and reconstructing tracks solely from the purified graph.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "plot_best_performances_squared_distance_max(\n",
    "    model=embedding_model,\n",
    "    path_or_config=CONFIG,\n",
    "    partition=\"minbias-sim10b-xdigi_v2.4_1498\",\n",
    "    # list_squared_distance_max=[0.015, 0.020],\n",
    "    list_squared_distance_max=[0.005, 0.010, 0.015, 0.020, 0.030, 0.040],\n",
    "    n_events=200,\n",
    "    seed=0,\n",
    "    builder=\"triplet\",\n",
    "    categories=[\n",
    "        mtb.category.category_velo_no_electrons,\n",
    "        mtb.category.category_long_only_electrons,\n",
    "        mtb.category.category_long_strange,\n",
    "    ],\n",
    "    with_err=False,\n",
    ");\n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we set `squared_distance_max_inference: 0.010` in the pipeline configuration to limit the number of edges in the graph.\n",
    "\n",
    "The value is applied when build the graph for each event in the training,\n",
    "validation and test datasets."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "graph_builder = run_embedding_inference(\n",
    "    CONFIG,\n",
    "    checkpoint=embedding_artifact_path,\n",
    "    partitions=[\"train\", \"val\", \"test\"],\n",
    "    # checkpoint=embedding_model,  # here directly use the model\n",
    "    reproduce=True,\n",
    "    # use_gpu=False,\n",
    ")\n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 4. Graph Neural Network to Classify the Edges and Edge-Edge connections"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Following the construction of graphs in the previous step, a Graph Neural Network (GNN)\n",
    "takes center stage to purify the grpah.\n",
    "Similar to the embedding learning, the training set is partitioned into 10 sub-epochs,\n",
    "and events are loaded lazily.\n",
    "\n",
    "The GNN comprises the following steps\n",
    "1. **Coordinate encoding**: Hit coordinates and edge coordinates are encoded into high-dimensinal representation\n",
    "using two distinct neural networks—namely, the node encoding network and the edge encoding network.\n",
    "2. **Message Passing**: A total of `n_graph_iters=6` message-passing iterations occur.\n",
    "During each iteration, two additional networks—the node network and the edge network—are employed\n",
    "to update the node and edge encodings based on propagated messages.\n",
    "3. **Edge Classification**: A dense neural network is applied to classify the edges.\n",
    "This network takes as input the node encodings of the edges and the edge encodings,\n",
    "assigning them a score denoted as $s_{\\text{edge}}$. This score ranges between 0 (indicating fakeness)\n",
    "and 1 (indicating genuinneess).\n",
    "4. **Edge Filtering**: Edges classified as fake are filtered out, requiring $s_{\\text{edge}} > s_{\\text{edge, min}} = 0.5$ for retention.\n",
    "6. **Triplet Formation**: Edge-edge connections, referred to as triplets, are built.\n",
    "7. **Triplet Classification**: A dense neural network is applied to classify the triplets,\n",
    "taking both the node encodings and edge encodings of the entities involved in each triplet into account.\n",
    "\n",
    "\n",
    "The edge-edge connections, also known as triplets, undergo filtration by imposing\n",
    "a minimum triplet score threshold denoted as $s_{\\text{triplet, min}}$.\n",
    "Subsequently, tracks can be constructed at the next stage,\n",
    "based on the filtered graph of interconnected edges.\n",
    "\n",
    "Triplet formation begins intentionally after `triplet_step = 500` training steps,\n",
    "ensuring it starts when edge classification performance has sufficiently improved\n",
    "to prevent excessive triplet generation.\n",
    "\n",
    "The GNN is trained with a [focal binary loss](https://pytorch.org/vision/main/generated/torchvision.ops.sigmoid_focal_loss.html#torchvision.ops.sigmoid_focal_loss)\n",
    "as the loss function.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "if run_training:\n",
    "    gnn_trainer, gnn_model = train_model(path_or_config=CONFIG, step=\"gnn\")"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, we find the path to the artifact file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "from utils.modelutils.checkpoint_utils import (\n",
    "    get_last_version_dir_from_config,\n",
    "    get_last_artifact,\n",
    ")\n",
    "\n",
    "gnn_version_dir = get_last_version_dir_from_config(step=\"gnn\", path_or_config=CONFIG)\n",
    "gnn_metric_path = os.path.join(gnn_version_dir, \"metrics.csv\")\n",
    "gnn_artifact_path = get_last_artifact(version_dir=gnn_version_dir)\n",
    "print(f\"{gnn_metric_path=}\")\n",
    "print(f\"{gnn_artifact_path=}\")\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To continue the training of the GNN, you may run the cell below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# import torch\n",
    "# from pytorch_lightning import Trainer\n",
    "# from pytorch_lightning.loggers import CSVLogger\n",
    "# from pytorch_lightning.callbacks import ModelCheckpoint, Callback\n",
    "\n",
    "# def continue_gnn_training(\n",
    "#     path_or_config: str | dict,\n",
    "# ) -> typing.Tuple[Trainer, InteractionGNN]:\n",
    "#     config = load_config(path_or_config=path_or_config)\n",
    "\n",
    "#     gnn_model = InteractionGNN.load_from_checkpoint(\n",
    "#         gnn_artifact_path, **config[\"gnn\"]\n",
    "#     )  # you may change `gnn_model`\n",
    "\n",
    "#     save_directory = os.path.abspath(\n",
    "#         os.path.join(config[\"common\"][\"artifact_directory\"], \"gnn\")\n",
    "#     )\n",
    "\n",
    "#     logger = CSVLogger(save_directory, name=config[\"common\"][\"experiment_name\"])\n",
    "\n",
    "#     gnn_trainer = Trainer(\n",
    "#         accelerator=\"gpu\" if torch.cuda.is_available() else \"cpu\",\n",
    "#         devices=1,\n",
    "#         max_epochs=150,  # you may increase the number of epochs\n",
    "#         logger=logger,\n",
    "#         reload_dataloaders_every_n_epochs=1,\n",
    "#         # callbacks=[EarlyStopping(monitor=\"val_loss\", mode=\"min\")]\n",
    "#     )\n",
    "\n",
    "#     with warnings.catch_warnings():\n",
    "#         warnings.filterwarnings(\n",
    "#             \"ignore\", message=\"None of the inputs have requires_grad=True.\"\n",
    "#         )\n",
    "#         gnn_trainer.fit(gnn_model, ckpt_path=gnn_artifact_path)\n",
    "#     return gnn_trainer, gnn_model\n",
    "\n",
    "# gnn_trainer, gnn_model = continue_gnn_training(CONFIG)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gnn_model = load_trained_model(path_or_config=CONFIG, step=\"gnn\")\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gnn_metrics_edge = checkpoint_utils.get_training_metrics(\n",
    "    gnn_metric_path, suffix=\"_edge\"\n",
    ")\n",
    "\n",
    "gnn_metrics_triplet = checkpoint_utils.get_training_metrics(\n",
    "    gnn_metric_path, suffix=\"_triplet\"\n",
    ")\n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {
    "jp-MarkdownHeadingCollapsed": true
   },
   "source": [
    "The loss, edge efficiency and edge purity during the training\n",
    "are plotted."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "perfplot_mpl.plot_loss(\n",
    "    metrics=gnn_metrics_edge,\n",
    "    path_or_config=CONFIG,\n",
    "    step=\"gnn_edge\",\n",
    ")\n",
    "perfplot_mpl.plot_loss(\n",
    "    metrics=gnn_metrics_triplet,\n",
    "    path_or_config=CONFIG,\n",
    "    step=\"gnn_triplet\",\n",
    ")\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "perfplot_mpl.plot_metric_epochs(\n",
    "    metric_name=\"eff\",\n",
    "    metrics=gnn_metrics_edge,\n",
    "    path_or_config=CONFIG,\n",
    "    step=\"gnn_edge\",\n",
    "    metric_label=\"Edge Efficiency\",\n",
    "    color=perfplot_mpl.partition_to_color[\"val\"],\n",
    ")\n",
    "perfplot_mpl.plot_metric_epochs(\n",
    "    metric_name=\"pur\",\n",
    "    metrics=gnn_metrics_edge,\n",
    "    path_or_config=CONFIG,\n",
    "    step=\"gnn_edge\",\n",
    "    metric_label=\"Edge Purity\",\n",
    "    color=perfplot_mpl.partition_to_color[\"val\"],\n",
    ")\n",
    "\n",
    "perfplot_mpl.plot_metric_epochs(\n",
    "    metric_name=\"eff\",\n",
    "    metrics=gnn_metrics_triplet,\n",
    "    path_or_config=CONFIG,\n",
    "    step=\"gnn_triplet\",\n",
    "    label=\"Edge Efficiency\",\n",
    "    color=perfplot_mpl.partition_to_color[\"val\"],\n",
    ")\n",
    "perfplot_mpl.plot_metric_epochs(\n",
    "    metric_name=\"pur\",\n",
    "    metrics=gnn_metrics_triplet,\n",
    "    path_or_config=CONFIG,\n",
    "    step=\"gnn_triplet\",\n",
    "    metric_label=\"Edge Purity\",\n",
    "    color=perfplot_mpl.partition_to_color[\"val\"],\n",
    ")\n",
    "\n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To determine the appropriate minimal edge score ($s_{\\text{edge, min}}$)\n",
    "and minimal triplet score ($s_{\\text{triplet, min}}$),\n",
    "these values are varied and their impact on edge and triplet efficiencies\n",
    "are assessed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "gnn_model.load_partition(\"minbias-sim10b-xdigi_v2.4_1496\")\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "results = perfplot_mpl.plot_edge_performance(\n",
    "    gnn_model,\n",
    "    path_or_config=CONFIG,\n",
    "    max_n_events=100,\n",
    "    edge_score_cuts=[\n",
    "        0.1,\n",
    "        0.2,\n",
    "        0.3,\n",
    "        0.4,\n",
    "        0.5,\n",
    "        0.6,\n",
    "        0.65,        \n",
    "        0.7,\n",
    "        0.75,\n",
    "        0.8,\n",
    "        0.9,\n",
    "    ]\n",
    ")\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(results)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dict_results = perfplot_mpl.plot_triplet_performance(\n",
    "    gnn_model,\n",
    "    path_or_config=CONFIG,\n",
    "    max_n_events=100,\n",
    "    edge_score_cut=0.40,\n",
    "    triplet_score_cuts=[\n",
    "        0.1,\n",
    "        0.2,\n",
    "        0.3,\n",
    "        0.35,\n",
    "        0.4,\n",
    "        0.45,\n",
    "        0.5,\n",
    "        0.7,\n",
    "        0.9,\n",
    "    ]\n",
    ")\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The minimal edge score $s_{\\text{edge, min}}$ 0.4\n",
    "is established.\n",
    "\n",
    "The track-finding performance across different $s_{\\text{triplet, min}}$ values\n",
    "is examined below.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from GNN.gnn_plots import plot_best_performances_score_cut_triplets\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "_, _, performances_for_various_score_cuts = plot_best_performances_score_cut_triplets(\n",
    "    model=gnn_model,\n",
    "    path_or_config=CONFIG,\n",
    "    partition=\"minbias-sim10b-xdigi_v2.4_1496\",\n",
    "    edge_score_cut=0.4,\n",
    "    triplet_score_cuts=[\n",
    "        0.1,\n",
    "        0.2,\n",
    "        0.22,\n",
    "        0.24,\n",
    "        0.26,\n",
    "        0.28,\n",
    "        0.3,\n",
    "        0.32,\n",
    "        0.34,\n",
    "        0.36,\n",
    "        0.38,\n",
    "        0.4,\n",
    "        0.5,\n",
    "        0.7,\n",
    "        0.9,\n",
    "    ],\n",
    "    n_events=100,\n",
    "    seed=0,\n",
    "    categories=[\n",
    "        mtb.category.category_velo_no_electrons,\n",
    "        mtb.category.category_long_only_electrons,\n",
    "        mtb.category.category_long_strange,\n",
    "    ],\n",
    "    track_metric_names=[\"ghost_rate\"],\n",
    "    with_err=False,\n",
    ")\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "performances_for_various_score_cuts"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "After evaluation, we opt for a minimal triplet score of 0.38 to reduce the ghost rate."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 5. Build track candidates from GNN"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To build tracks, we follow a procedure detailed in slides 16 and 17 of\n",
    "[my presentation at the WP2 reconstruction meeting](https://indico.cern.ch/event/1299280/contributions/5564527/attachments/2713082/4711760/CORRECTED%20Enhancements%20in%20ETX4VELO_%20Improved%20Graph%20Neural%20Network%20for%20Track%20Finding%20in%20the%20Velo.pdf).\n",
    "\n",
    "Using a minimal edge score of 0.4 and a minimal triplet score of 0.38, we construct tracks for a second test sample."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "build_track_candidates(\n",
    "    CONFIG, partitions=[\"minbias-sim10b-xdigi_v2.4_1498\"], reproduce=True,\n",
    ")\n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 6. Evaluate track candidates on unseen data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Subsequently, the tracks found by ETX4velo are matched to the particles,\n",
    "and the physics performance evaluation is performed. This step\n",
    "is performed using the [MonteTracko library](https://gitlab.cern.ch/gdl4hep/montetracko/).\n",
    "\n",
    "The physics performance is compared with the baseline algorithm in Allen."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from scripts.evaluation.compare_allen_vs_etx4velo import compare_etx4velo_vs_allen\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "compare_etx4velo_vs_allen(\n",
    "    path_or_config=CONFIG,\n",
    "    test_dataset_name=\"minbias-sim10b-xdigi_v2.4_1498\",\n",
    "    categories=[\n",
    "        mtb.category.category_velo_no_electrons,\n",
    "        mtb.category.category_long_only_electrons,\n",
    "        mtb.category.category_long_strange,\n",
    "    ],\n",
    "    allen_report=True,\n",
    "    table_report=True,\n",
    "    with_err=False,\n",
    ")\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Three figures are plotted, with each figure corresponding to one of the following categories:\n",
    "- Velo-reconstructible particles, excluding electrons\n",
    "- Velo- and SciFi-reconstructible electrons\n",
    "- Velo- and SciFi-reconstructible particles that originated from a strange particle decay\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The results shown during the [WP2 reconstruction meeting](https://indico.cern.ch/event/1299280/#5-enhancements-in-etx4velo-imp)\n",
    "were obtained using the `pipeline_configs/focal-loss-nopid-triplets-embedding-3-withspillover-new.yaml`\n",
    "pipeline configuration."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  },
  "vscode": {
   "interpreter": {
    "hash": "31f2aee4e71d21fbe5cf8b01ff0e069b9275f58929596ceb00d14d90e3e16cd6"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
