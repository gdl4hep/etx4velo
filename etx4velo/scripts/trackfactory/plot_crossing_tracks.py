#!/usr/bin/env python3
"""A script that plots tracks that cross, with various configurations.
"""
from argparse import ArgumentParser
import os
import os.path as op
import json

from tqdm.auto import tqdm
import torch

from montetracko.lhcb.category import reconstructible_velo
import dfutils

from utils.scriptutils.parser import add_predefined_arguments
from utils.commonutils.config import get_performance_directory_experiment, load_config
from utils.commonutils.ctests import get_test_batch_paths
from utils.plotutils.plotconfig import configure_matplotlib

configure_matplotlib()

if __name__ == "__main__":
    parser = ArgumentParser("Plot tracks whose first hit is the last hit of another.")
    add_predefined_arguments(
        parser, ["pipeline_config", "test_dataset_name", "output_dir"]
    )
    parser.add_argument(
        "-ct",
        "--crossing_type",
        help="Number of tracks",
        choices=["start_is_end", "cross", "start_from", "end_at"],
        required=True,
    )
    parser.add_argument(
        "-n", "--n_tracks", help="Number of tracks", type=int, default=8
    )
    parser.add_argument(
        "--count",
        help="Whether to count the number of tracks per event",
        action="store_true",
    )
    parsed_args = parser.parse_args()
    test_dataset_name: str = parsed_args.test_dataset_name
    pipeline_config_path: str = parsed_args.pipeline_config
    n_tracks: int = parsed_args.n_tracks
    output_dir: str = (
        op.join(
            get_performance_directory_experiment(pipeline_config_path), "trackfactory"
        )
        if parsed_args.output_dir is None
        else parsed_args.output_dir
    )
    count: bool = parsed_args.count
    crossing_type: str = parsed_args.crossing_type

    inpaths = get_test_batch_paths(
        experiment_name=load_config(pipeline_config_path)["common"]["experiment_name"],
        stage="embedding_processed",
        test_dataset_name=test_dataset_name,
    )

    n_plotted_tracks = 0
    n_crossing_tracks = 0
    for inpath in tqdm(inpaths):
        batch = torch.load(inpath, map_location="cpu")
        event_id_str = batch["event_str"]

        df_hits_particles_all = dfutils.get_df_hits_particles_from_batch(batch=batch)
        df_hits_particles_all = dfutils.add_particle_information(
            truncated_path=batch["truncated_path"],
            df_hits_particles=df_hits_particles_all,
            particle_columns=["eta", "has_velo"],
        )

        df_true_edges = dfutils.get_df_edges(
            edge_indices=batch["signal_true_edges"].numpy(),
            df_hits_particles=df_hits_particles_all[
                ["hit_idx", "x", "y", "z", "particle_id"]
            ],
        )

        df_hits_particles = reconstructible_velo.filter(df_hits_particles_all).copy()

        if crossing_type == "start_is_end":
            df_hits_particles = dfutils.compute_n_particles_hit(df_hits_particles)

            df_hits_particles_special = df_hits_particles[
                df_hits_particles["n_particles_hit"] == 2
            ]
            # Start of a track is the end of another one
            end_hit_indices = df_hits_particles_special[
                df_hits_particles_special["plane"]
                == df_hits_particles_special["max_plane"]
            ]["hit_idx"]
            start_hit_indices = df_hits_particles_special[
                df_hits_particles_special["plane"]
                == df_hits_particles_special["min_plane"]
            ]["hit_idx"]
            interesting_hit_indices = end_hit_indices[
                end_hit_indices.isin(start_hit_indices)
            ].unique()
        elif crossing_type == "cross":
            df_hits_particles_special = df_hits_particles[
                (df_hits_particles["plane"] != df_hits_particles["max_plane"])
                & (df_hits_particles["plane"] != df_hits_particles["min_plane"])
            ].copy()
            df_hits_particles_special = dfutils.compute_n_particles_hit(
                df_hits_particles_special
            )

            df_hits_particles_special = df_hits_particles_special[
                df_hits_particles_special["n_particles_hit"] == 2
            ]
            # 2 particles that cross, then split
            interesting_hit_indices = df_hits_particles_special["hit_idx"].unique()

        elif crossing_type == "start_from":
            df_hits_particles = dfutils.compute_n_particles_hit(df_hits_particles)

            df_hits_particles_special = df_hits_particles[
                df_hits_particles["n_particles_hit"] == 2
            ]
            start_hit_indices = df_hits_particles_special[
                df_hits_particles_special["plane"]
                == df_hits_particles_special["min_plane"]
            ]["hit_idx"]
            other_hit_indices = df_hits_particles_special[
                (
                    df_hits_particles_special["plane"]
                    != df_hits_particles_special["max_plane"]
                )
                & (
                    df_hits_particles_special["plane"]
                    != df_hits_particles_special["min_plane"]
                )
            ]["hit_idx"]
            interesting_hit_indices = start_hit_indices[
                start_hit_indices.isin(other_hit_indices)
            ].unique()

        elif crossing_type == "end_at":
            df_hits_particles = dfutils.compute_n_particles_hit(df_hits_particles)

            df_hits_particles_special = df_hits_particles[
                df_hits_particles["n_particles_hit"] == 2
            ]
            end_hit_indices = df_hits_particles_special[
                df_hits_particles_special["plane"]
                == df_hits_particles_special["max_plane"]
            ]["hit_idx"]
            other_hit_indices = df_hits_particles_special[
                (
                    df_hits_particles_special["plane"]
                    != df_hits_particles_special["max_plane"]
                )
                & (
                    df_hits_particles_special["plane"]
                    != df_hits_particles_special["min_plane"]
                )
            ]["hit_idx"]
            interesting_hit_indices = end_hit_indices[
                end_hit_indices.isin(other_hit_indices)
            ].unique()
        else:
            raise ValueError(f"`crossing_type` {crossing_type} is not recognised.")

        df_true_edges = dfutils.compute_edge_counts(df_true_edges)
        interesting_hit_indices = dfutils.no_shared_edges(
            interesting_hit_indices, df_edges=df_true_edges
        )

        df_hits_particles_special = df_hits_particles_special[
            df_hits_particles_special["hit_idx"].isin(interesting_hit_indices)
        ]
        df_hits_particles_special = dfutils.compute_n_particles_hit(
            df_hits_particles_special
        )

        df_hits_particles_special = df_hits_particles_special[
            df_hits_particles_special["n_particles_hit"] == 2
        ]
        particle_ids_to_plot = df_hits_particles_special["particle_id"].unique()
        n_crossing_tracks += df_hits_particles_special["hit_idx"].nunique()

        if n_plotted_tracks < n_tracks:
            n_plotted_tracks += dfutils.plot_connected_particle_ids(
                particle_ids=particle_ids_to_plot,
                df_hits_particles=df_hits_particles_all,
                df_true_edges=df_true_edges,
                n_plots_max=n_tracks - n_plotted_tracks,
                output_wpath=op.join(
                    output_dir,
                    f"{test_dataset_name}_{crossing_type}_{event_id_str}_{{particle_id}}",
                ),
                lhcb=True,
            )

        if not count and n_plotted_tracks >= n_tracks:
            break

    if count:
        n_events = len(inpaths)
        os.makedirs(output_dir, exist_ok=True)
        json_path = op.join(
            output_dir, f"{test_dataset_name}_{crossing_type}_tracks_count.json"
        )
        with open(json_path, "w") as json_file:
            json.dump(
                {
                    "n_events": n_events,
                    f"n_{crossing_type}_tracks": n_crossing_tracks,
                    f"prop_{crossing_type}_tracks": n_crossing_tracks / n_events,
                },
                json_file,
            )
            print("Json file saved in", json_path)
