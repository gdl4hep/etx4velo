#!/usr/bin/env python3
"""A script that plots electron-positron tracks that share their first hits
before parting ways.
"""
from argparse import ArgumentParser
import os
import os.path as op
import json

from tqdm.auto import tqdm
import torch

from montetracko.lhcb.category import category_velo
import dfutils

from utils.scriptutils.parser import add_predefined_arguments
from utils.commonutils.config import get_performance_directory_experiment, load_config
from utils.commonutils.ctests import get_test_batch_paths
from utils.plotutils.plotconfig import configure_matplotlib

configure_matplotlib()

if __name__ == "__main__":
    parser = ArgumentParser(
        "Plot electron tracks that share hits with another electron."
    )
    add_predefined_arguments(
        parser, ["pipeline_config", "test_dataset_name", "output_dir"]
    )
    parser.add_argument(
        "-n", "--n_tracks", help="Number of tracks", type=int, default=5
    )
    parser.add_argument(
        "--count",
        help="Whether to count the number of tracks per event",
        action="store_true",
    )
    parsed_args = parser.parse_args()
    test_dataset_name: str = parsed_args.test_dataset_name
    pipeline_config_path: str = parsed_args.pipeline_config
    n_tracks: int = parsed_args.n_tracks
    output_dir: str = (
        op.join(
            get_performance_directory_experiment(pipeline_config_path),
            "trackfactory",
        )
        if parsed_args.output_dir is None
        else parsed_args.output_dir
    )
    count: bool = parsed_args.count

    inpaths = get_test_batch_paths(
        experiment_name=load_config(pipeline_config_path)["common"]["experiment_name"],
        stage="embedding_processed",
        test_dataset_name=test_dataset_name,
    )

    n_electrons = 0
    n_connected_electrons = 0
    n_plotted_tracks = 0
    for inpath in tqdm(inpaths):
        batch = torch.load(inpath, map_location="cpu")
        event_id_str = batch["event_str"]

        df_hits_particles_all = dfutils.get_df_hits_particles_from_batch(batch=batch)
        df_hits_particles_all = dfutils.add_particle_information(
            truncated_path=batch["truncated_path"],
            df_hits_particles=df_hits_particles_all,
            particle_columns=["eta", "has_velo", "pid"],
        )

        df_true_edges = dfutils.get_df_edges(
            edge_indices=batch["signal_true_edges"].numpy(),
            df_hits_particles=df_hits_particles_all.drop(["x", "y", "z"], axis=1),
        )

        df_hits_particles = category_velo.filter(df_hits_particles_all).copy()
        df_hits_particles = df_hits_particles[(df_hits_particles["pid"].abs() == 11)]
        df_hits_particles = dfutils.compute_n_particles_hit(df_hits_particles)

        df_hits_particles_special = df_hits_particles[
            df_hits_particles["n_particles_hit"] >= 2
        ]

        particle_ids_to_plot = df_hits_particles_special["particle_id"].unique()
        n_connected_electrons += particle_ids_to_plot.shape[0]
        n_electrons += df_hits_particles["particle_id"].nunique()

        if n_plotted_tracks < n_tracks:
            n_plotted_tracks += dfutils.plot_connected_particle_ids(
                particle_ids=particle_ids_to_plot,
                df_hits_particles=df_hits_particles,
                df_true_edges=df_true_edges,
                n_plots_max=n_tracks - n_plotted_tracks,
                output_wpath=op.join(
                    output_dir,
                    f"{test_dataset_name}_shared_electron_{event_id_str}_{{particle_id}}",
                ),
                lhcb=True,
            )

        if not count and n_plotted_tracks >= n_tracks:
            break

    if count:
        n_events = len(inpaths)
        os.makedirs(output_dir, exist_ok=True)
        json_path = op.join(
            output_dir, f"{test_dataset_name}_shared_electron_tracks_count.json"
        )
        with open(json_path, "w") as json_file:
            json.dump(
                {
                    "n_electrons": n_electrons,
                    "n_connected_electrons": n_connected_electrons,
                    "prop_connected_electrons": n_connected_electrons / n_electrons,
                },
                json_file,
            )
            print("Json file saved in", json_path)
