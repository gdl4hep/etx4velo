#!/usr/bin/env python3
"""A script that runs the preprocessing of a test sample.
"""
from argparse import ArgumentParser

from Processing.run_processing import run_processing_from_config
from utils.scriptutils.parser import add_predefined_arguments

if __name__ == "__main__":
    parser = ArgumentParser("Run the processing of all the test sample.")

    add_predefined_arguments(
        parser, ["pipeline_config", "test_dataset_name", "test_config", "reproduce"]
    )

    parsed_args = parser.parse_args()
    test_dataset_name: str = parsed_args.test_dataset_name
    pipeline_config_path: str = parsed_args.pipeline_config
    reproduce: bool = parsed_args.reproduce

    print(f"Run processing of {test_dataset_name}")
    run_processing_from_config(
        path_or_config=pipeline_config_path,
        test_dataset_name=test_dataset_name,
        reproduce=reproduce,
    )
