#!/usr/bin/env python3
"""A script that plots various distributions of hits on a test sample.
"""
import os.path as op
from argparse import ArgumentParser

import numpy as np
import matplotlib.pyplot as plt

from utils.scriptutils.parser import add_predefined_arguments
from utils.loaderutils.preprocessing import load_dataframes
from utils.commonutils.config import cdirs
from utils.plotutils.plotconfig import configure_matplotlib
from utils.plotutils.plotools import save_fig, add_text

from montetracko.lhcb.category import category_velo

configure_matplotlib()


if __name__ == "__main__":
    parser = ArgumentParser("Plot distibutions of hits of a test sample.")
    add_predefined_arguments(parser, ["test_dataset_name", "output_dir", "detector"])
    parsed_args = parser.parse_args()
    test_dataset_name: str = parsed_args.test_dataset_name
    output_dir: str = (
        parsed_args.output_dir
        if parsed_args.output_dir is not None
        else op.join(cdirs.analysis_directory, "data_distribution")
    )
    detector: str = parsed_args.detector

    df_hits_particles, df_particles = load_dataframes(
        indir=op.join(cdirs.reference_directory, test_dataset_name, "xdigi2csv"),
        particles_columns=["eta", "has_velo"],
        hits_particles_columns=["plane"],
        use_run_number=True,
        **cdirs.get_filenames_from_detector(detector=detector),
    )

    df_unique_hits = df_hits_particles.drop_duplicates(["event_id", "hit_id"])
    fake_mask = df_unique_hits["particle_id"] == 0

    event_ids = df_unique_hits["event_id"].unique()

    series_n_fake_hits_per_event = (
        df_unique_hits[fake_mask]
        .groupby("event_id")["hit_id"]
        .count()
        .reindex(event_ids, fill_value=0)
    )
    series_n_true_hits_per_event = (
        df_unique_hits[~fake_mask]
        .groupby("event_id")["hit_id"]
        .count()
        .reindex(event_ids, fill_value=0)
    )

    has_noise = series_n_fake_hits_per_event.sum() > 0

    if has_noise:
        min_range = min(
            series_n_fake_hits_per_event.min(), series_n_true_hits_per_event.min()
        )
        max_range = max(
            series_n_fake_hits_per_event.max(), series_n_true_hits_per_event.max()
        )
    else:
        min_range = series_n_true_hits_per_event.min()
        max_range = series_n_true_hits_per_event.max()

    hist_n_true_hits, bin_edges = np.histogram(
        series_n_true_hits_per_event,
        density=True,
        range=(min_range, max_range),
        bins=70,
    )
    if has_noise:
        hist_n_fake_hits, _ = np.histogram(
            series_n_fake_hits_per_event,
            bins=bin_edges,
            density=True,
        )
    else:
        hist_n_fake_hits = None

    bin_centers = (bin_edges[1:] + bin_edges[:-1]) / 2
    bin_widths = bin_edges[1:] - bin_edges[:-1]

    array_prop_fake_hits = series_n_fake_hits_per_event / (
        series_n_true_hits_per_event + series_n_fake_hits_per_event
    )
    prop_fake_hits_mean = array_prop_fake_hits.mean()
    prop_fake_hits_std = array_prop_fake_hits.std()

    fig, ax = plt.subplots(figsize=(8, 6))
    ax.set_xlabel("\\# hits per event")
    ax.set_ylabel("Density")
    ax.grid(color="grey", alpha=0.2)
    ax.bar(
        x=bin_centers,
        height=hist_n_true_hits,
        width=bin_widths,
        color="g",
        alpha=0.5,
        label="Genuine",
    )

    if has_noise:
        assert hist_n_fake_hits is not None
        ax.bar(
            x=bin_centers,
            height=hist_n_fake_hits,
            width=bin_widths,
            color="r",
            alpha=0.5,
            label=(f"Noise $\\left({prop_fake_hits_mean * 100:.1f} \\%\\right)$"),
        )
    add_text(ax, ha="right", y=0.7)
    ax.legend()
    save_fig(
        fig,
        path=op.join(output_dir, f"hist_n_hits_{test_dataset_name}"),
    )
    plt.close(fig)

    particle_counts_per_event = (
        df_particles[df_particles["has_velo"]]
        .groupby("event")["particle_id"]
        .count()
        .to_numpy()
    )
    fig, ax = plt.subplots(figsize=(8, 6))
    ax.set_xlabel("\\# reconstructible particles per event")
    ax.set_ylabel("Density")
    ax.grid(color="grey", alpha=0.2)
    ax.hist(particle_counts_per_event, color="purple", bins=70, range=(0, 400))
    add_text(ax, ha="right", va="top")
    save_fig(
        fig,
        path=op.join(output_dir, f"hist_n_reco_particles_{test_dataset_name}"),
    )
    plt.close(fig)

    df_particles_velo = category_velo.filter(dataframe=df_particles)

    particle_counts_per_event = (
        df_particles_velo[df_particles_velo["has_velo"]]
        .groupby("event")["particle_id"]
        .count()
        .to_numpy()
    )
    fig, ax = plt.subplots(figsize=(8, 6))
    ax.set_xlabel("\\# reconstructible particles in acceptance\nper event")
    ax.set_ylabel("Density")
    ax.grid(color="grey", alpha=0.2)
    ax.hist(particle_counts_per_event, color="purple", bins=70, range=(0, 400))
    add_text(ax, ha="right", va="top")
    save_fig(
        fig,
        path=op.join(output_dir, f"hist_n_reco_acc_particles_{test_dataset_name}"),
    )
    plt.close(fig)
