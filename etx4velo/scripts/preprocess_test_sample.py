#!/usr/bin/env python3
"""A script that runs the preprocessing of a test sample.
"""
from argparse import ArgumentParser

from Preprocessing.run_preprocessing import run_preprocessing_test_dataset
from utils.scriptutils.parser import add_predefined_arguments

if __name__ == "__main__":
    parser = ArgumentParser("Run the preprocessing of all the test sample.")

    add_predefined_arguments(parser, ["test_dataset_name", "test_config", "detector"])

    parsed_args = parser.parse_args()
    test_dataset_name = parsed_args.test_dataset_name
    test_config_path: str = parsed_args.test_config
    detector: str = parsed_args.detector

    print(f"Run preprocessing of {test_dataset_name}")
    run_preprocessing_test_dataset(
        test_dataset_name=test_dataset_name,
        path_or_config_test=test_config_path,
        detector=detector,
        reproduce=True,
        raise_enough_events=False,
    )
