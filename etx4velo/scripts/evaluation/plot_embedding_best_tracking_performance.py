#!/usr/bin/env python3
"""A script that plots the metric saved during the learning.
"""

from argparse import ArgumentParser
import montetracko.lhcb as mtb

from Embedding.embedding_plots import plot_best_performances_squared_distance_max

from utils.scriptutils.parser import add_predefined_arguments
from utils.plotutils.plotconfig import configure_matplotlib
from pipeline import load_trained_model

configure_matplotlib()


if __name__ == "__main__":
    parser = ArgumentParser(
        "Plot the best tracking performance "
        "as a function of the maximal squared distance."
    )
    add_predefined_arguments(
        parser, ["pipeline_config", "test_dataset_name", "output_path"]
    )

    parser.add_argument(
        "-b",
        "--builder",
        help="Builder used to build the tracks after the GNN.",
        choices=["default", "triplet"],
        default="triplet",
    )
    parsed_args = parser.parse_args()
    pipeline_config_path: str = parsed_args.pipeline_config
    test_dataset_name: str = parsed_args.test_dataset_name
    output_wpath: str | None = parsed_args.output_path
    builder: str = parsed_args.builder

    embedding_model = load_trained_model(
        path_or_config=pipeline_config_path, step="embedding"
    )

    plot_best_performances_squared_distance_max(
        model=embedding_model,
        path_or_config=pipeline_config_path,
        partition=test_dataset_name,
        list_squared_distance_max=[
            0.005,
            0.0075,
            0.010,
            0.0125,
            0.015,
            0.0175,
            0.020,
            0.030,
            0.040,
        ],
        n_events=200,
        seed=0,
        builder=builder,
        categories=[
            mtb.category.category_only_velo_no_electrons,
            mtb.category.category_only_velo_only_electrons,
            mtb.category.category_long_no_electrons,
            mtb.category.category_long_only_electrons,
            mtb.category.category_long_strange,
        ],
        category_name_to_color={
            mtb.category.category_only_velo_no_electrons.name: "#fe9929",
            mtb.category.category_only_velo_only_electrons.name: "#cc4c02",
            mtb.category.category_long_no_electrons.name: "#74a9cf",
            mtb.category.category_long_only_electrons.name: "#0570b0",
            mtb.category.category_long_strange.name: "purple",
        },
        output_path=output_wpath,
        same_fig=False,
        lhcb=True,
        legend_inside=True,
    )
