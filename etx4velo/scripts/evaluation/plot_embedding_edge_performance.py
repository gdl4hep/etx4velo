#!/usr/bin/env python3
"""A script that plots the edge performance (edge purity and efficiency)
as a function of the maximal squared distance.
"""

from argparse import ArgumentParser
import os.path as op

from Embedding.embedding_plots import (
    plot_embedding_performance_given_squared_distance_max_k_max,
)

from utils.scriptutils.parser import add_predefined_arguments
from utils.commonutils.config import cdirs, load_config
from utils.plotutils.plotconfig import configure_matplotlib
from pipeline import load_trained_model

configure_matplotlib()

if __name__ == "__main__":
    parser = ArgumentParser("Plot plots to explain the embedding.")
    add_predefined_arguments(
        parser, ["pipeline_config", "test_dataset_name", "output_dir"]
    )
    parsed_args = parser.parse_args()
    pipeline_config_path: str = parsed_args.pipeline_config
    test_dataset_name: str = parsed_args.test_dataset_name
    output_dir: str = (
        parsed_args.output_dir
        if parsed_args.output_dir is not None
        else op.join(
            cdirs.performance_directory,
            load_config(pipeline_config_path)["common"]["experiment_name"],
            "embedding",
        )
    )

    embedding_model = load_trained_model(
        path_or_config=pipeline_config_path, step="embedding"
    )

    plot_embedding_performance_given_squared_distance_max_k_max(
        model=embedding_model,
        squared_distance_max=[
            0.005,
            0.0075,
            0.010,
            0.0125,
            0.015,
            0.0175,
            0.020,
            0.030,
            0.040,
        ],
        n_events=200,
        partitions={test_dataset_name: None},
        output_wpath=op.join(
            output_dir, "{metric_name}_vs_squared_maximal_distance_" + test_dataset_name
        ),
        lhcb=True,
    )
