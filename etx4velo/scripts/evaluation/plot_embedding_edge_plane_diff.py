#!/usr/bin/env python3
"""A script that plots statistics about the edges build by the embedding stage.
"""
import typing
import os.path as op
from argparse import ArgumentParser

from tqdm.auto import tqdm
from joblib import Parallel, delayed
import numpy as np
import numpy.typing as npt
import torch
from torch_geometric.data import Data
import matplotlib.pyplot as plt

from utils.scriptutils.parser import add_predefined_arguments
from utils.commonutils.config import cdirs, load_config
from utils.commonutils.ctests import get_test_batch_paths
from utils.plotutils.plotconfig import configure_matplotlib
from utils.plotutils import plotools

configure_matplotlib()


if __name__ == "__main__":
    parser = ArgumentParser(
        "Plot statistics about the edges build by the embedding stage."
    )
    add_predefined_arguments(
        parser, ["pipeline_config", "test_dataset_name", "output_path", "n_workers"]
    )
    parsed_args = parser.parse_args()
    pipeline_config_path: str = parsed_args.pipeline_config
    test_dataset_name: str = parsed_args.test_dataset_name
    experiment_name = load_config(pipeline_config_path)["common"]["experiment_name"]
    output_path: str = (
        parsed_args.output_path
        if parsed_args.output_path is not None
        else op.join(
            cdirs.performance_directory,
            experiment_name,
            f"embedding_plane_diff_{test_dataset_name}",
        )
    )
    n_workers: int = parsed_args.n_workers

    inpaths = get_test_batch_paths(
        experiment_name=experiment_name,
        stage="embedding_processed",
        test_dataset_name=test_dataset_name,
    )

    max_plane_diff = 2
    bin_centers = np.arange(1, max_plane_diff + 1)
    bin_edges = np.arange(1, max_plane_diff + 2) - 0.5

    def get_array_n_neighbours(inpath: str) -> npt.NDArray:
        batch: Data = torch.load(inpath, map_location="cpu")
        array_plane_diffs = (
            batch["plane"][batch["edge_index"][1]]
            - batch["plane"][batch["edge_index"][0]]
        ).numpy()
        plane_diff_hist, _ = np.histogram(
            array_plane_diffs, bins=bin_edges, density=False
        )
        return plane_diff_hist

    list_plane_diff_hists: typing.List[npt.NDArray] = Parallel(n_jobs=n_workers)(  # type: ignore
        delayed(get_array_n_neighbours)(inpath=inpath) for inpath in tqdm(inpaths)
    )

    plane_diff_hist: npt.NDArray = sum(list_plane_diff_hists)  # type: ignore

    fig, ax = plt.subplots(figsize=(8, 6))
    ax.set_xlabel("Plane difference")
    ax.set_ylabel("Proportion of edges")
    ax.bar(
        x=bin_centers,
        height=plane_diff_hist / plane_diff_hist.sum(),
        width=1.0,
        color="grey",
    )
    ax.locator_params(axis="x", integer=True)
    ax.grid(color="grey", alpha=0.3)
    plotools.pad_on_top(ax)
    plotools.add_text(ax, ha="right", va="top")
    plotools.save_fig(fig, path=output_path)
