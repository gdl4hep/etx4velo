#!/usr/bin/env python3
"""A script that plots statistics about the edges build by the embedding stage.
"""
import typing
import os.path as op
from argparse import ArgumentParser

from tqdm.auto import tqdm
from joblib import Parallel, delayed
import numpy as np
import numpy.typing as npt
import pandas as pd
import torch
from torch_geometric.data import Data
import matplotlib.pyplot as plt

from utils.scriptutils.parser import add_predefined_arguments
from utils.commonutils.config import cdirs, load_config
from utils.commonutils.ctests import get_test_batch_paths
from utils.plotutils.plotconfig import configure_matplotlib
from utils.plotutils.plotools import save_fig, add_text

configure_matplotlib()


if __name__ == "__main__":
    parser = ArgumentParser(
        "Plot statistics about the edges build by the embedding stage."
    )
    add_predefined_arguments(
        parser, ["pipeline_config", "test_dataset_name", "output_path", "n_workers"]
    )

    parsed_args = parser.parse_args()
    pipeline_config_path: str = parsed_args.pipeline_config
    test_dataset_name: str = parsed_args.test_dataset_name
    experiment_name = load_config(pipeline_config_path)["common"]["experiment_name"]
    output_path: str = (
        parsed_args.output_path
        if parsed_args.output_path is not None
        else op.join(
            cdirs.performance_directory,
            experiment_name,
            f"embedding_n_neighbours_{test_dataset_name}",
        )
    )
    n_workers: int = parsed_args.n_workers

    inpaths = get_test_batch_paths(
        experiment_name=experiment_name,
        stage="embedding_processed",
        test_dataset_name=test_dataset_name,
    )

    def get_array_n_neighbours(inpath: str) -> npt.NDArray:
        batch: Data = torch.load(inpath, map_location="cpu")
        df_edges = pd.DataFrame(
            {
                "edge_idx_left": batch["edge_index"][0].numpy(),
                "edge_idx_right": batch["edge_index"][1].numpy(),
            }
        )
        return df_edges.groupby("edge_idx_left")["edge_idx_right"].count().to_numpy()

    list_array_n_neighbours: typing.List[npt.NDArray] = Parallel(n_jobs=n_workers)(  # type: ignore
        delayed(get_array_n_neighbours)(inpath=inpath) for inpath in tqdm(inpaths)
    )

    array_n_neighbours = np.concatenate(list_array_n_neighbours, axis=0)
    bin_edges = np.arange(array_n_neighbours.min(), array_n_neighbours.max() + 2) - 0.5

    fig, ax = plt.subplots(figsize=(8, 6))
    ax.set_xlabel("Number of neighbours per hit")
    ax.set_ylabel("Proportion of hits")
    ax.hist(x=array_n_neighbours, bins=bin_edges, density=True, color="grey")
    ax.locator_params(axis="x", integer=True)
    ax.grid(color="grey", alpha=0.3)
    ax.grid(color="grey", alpha=0.3)
    add_text(ax, ha="right", va="top")
    save_fig(fig, path=output_path)
