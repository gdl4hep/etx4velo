#!/usr/bin/env python3
"""A script that plots metrics to show how the training went.
"""
from argparse import ArgumentParser
import os.path as op

from utils.modelutils import checkpoint_utils
from utils.scriptutils.parser import add_predefined_arguments
from utils.plotutils import performance_mpl as perfplot_mpl
from utils.plotutils.plotconfig import configure_matplotlib

configure_matplotlib()


if __name__ == "__main__":
    parser = ArgumentParser("Plot metrics to show how the training went.")
    add_predefined_arguments(parser, ["pipeline_config", "output_dir"])
    parser.add_argument("-s", "--step", help="Pipeline step.", required=True)
    parser.add_argument(
        "--suffix",
        help="Suffix to add to the metric to plots",
        required=False,
        default="",
    )
    parsed_args = parser.parse_args()
    pipeline_config_path = parsed_args.pipeline_config
    output_dir: str = parsed_args.output_dir
    suffix: str = parsed_args.suffix
    step: str = parsed_args.step

    version_dir = checkpoint_utils.get_last_version_dir_from_config(
        step=step, path_or_config=pipeline_config_path
    )
    embedding_metrics = checkpoint_utils.get_training_metrics(
        op.join(version_dir, "metrics.csv"),
        suffix=suffix,
    )
    perfplot_mpl.plot_loss(
        embedding_metrics,
        lhcb=True,
        output_path=op.join(output_dir, f"{step}_loss{suffix}"),
    )

    if suffix == "_triplet":
        element_label = "Triplet"
    else:
        element_label = "Edge"

    perfplot_mpl.plot_metric_epochs(
        "eff",
        embedding_metrics,
        f"{element_label} efficiency",
        step=step,
        color=perfplot_mpl.partition_to_color["val"],
        output_path=op.join(output_dir, f"{step}_eff{suffix}"),
        lhcb=True,
    )
    perfplot_mpl.plot_metric_epochs(
        "pur",
        embedding_metrics,
        f"{element_label} purity",
        step=step,
        color=perfplot_mpl.partition_to_color["val"],
        output_path=op.join(output_dir, f"{step}_pur{suffix}"),
        lhcb=True,
    )
