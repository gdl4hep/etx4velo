#!/usr/bin/env python3
"""A script that plots the metric saved during the learning.
"""
import warnings
from argparse import ArgumentParser

import montetracko.lhcb as mtb

from GNN.models.triplet_interaction_gnn import TripletGNNBase
from GNN.gnn_plots import plot_best_performances_score_cut_triplets
from pipeline import load_trained_model

from utils.scriptutils.parser import add_predefined_arguments
from utils.commonutils.config import load_config
from utils.plotutils.plotconfig import configure_matplotlib


configure_matplotlib()


warnings.filterwarnings(
    "ignore", "None of the inputs have requires_grad=True. Gradients will be None"
)


if __name__ == "__main__":
    parser = ArgumentParser(
        "Plot the best tracking performance "
        "as a function of the maximal squared distance."
    )
    add_predefined_arguments(parser, ["pipeline_config", "test_dataset_name"])
    parser.add_argument(
        "-o",
        "--output_wpath",
        help="Output path where to save the plot, with placeholder `{metric_name}`.",
        required=False,
    )
    parser.add_argument(
        "-n",
        "--n_events",
        help="Maximal number of events to use.",
        type=int,
        default=200,
        required=False,
    )
    parsed_args = parser.parse_args()
    pipeline_config_path: str = parsed_args.pipeline_config
    test_dataset_name: str = parsed_args.test_dataset_name
    output_wpath: str | None = parsed_args.output_wpath
    n_events: int = parsed_args.n_events

    gnn_model = load_trained_model(path_or_config=pipeline_config_path, step="gnn")
    assert isinstance(gnn_model, TripletGNNBase), (
        "This script only support the `TripletInteraction GNN for now, "
        f"but the model is {type(gnn_model).__name__}"
    )

    plot_best_performances_score_cut_triplets(
        model=gnn_model,
        partition=test_dataset_name,
        edge_score_cut=load_config(pipeline_config_path)["track_building"][
            "edge_score_cut"
        ],
        triplet_score_cuts=[
            0.1,
            0.2,
            0.22,
            0.24,
            0.26,
            0.28,
            0.3,
            0.32,
            0.34,
            0.36,
            0.38,
            0.4,
            0.5,
            0.7,
        ],
        n_events=n_events,
        seed=0,
        categories=[
            mtb.category.category_only_velo_no_electrons,
            mtb.category.category_only_velo_only_electrons,
            mtb.category.category_long_no_electrons,
            mtb.category.category_long_only_electrons,
            mtb.category.category_long_strange,
        ],
        category_name_to_color={
            mtb.category.category_only_velo_no_electrons.name: "#fe9929",
            mtb.category.category_only_velo_only_electrons.name: "#cc4c02",
            mtb.category.category_long_no_electrons.name: "#74a9cf",
            mtb.category.category_long_only_electrons.name: "#0570b0",
            mtb.category.category_long_strange.name: "purple",
        },
        track_metric_names=["ghost_rate"],
        same_fig=False,
        lhcb=True,
        output_path=output_wpath,
    )
