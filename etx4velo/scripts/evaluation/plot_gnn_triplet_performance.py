#!/usr/bin/env python3
"""A script that plots the metrics saved during the learning.
"""
import warnings
from argparse import ArgumentParser

from GNN.models.triplet_interaction_gnn import TripletInteractionGNN
from pipeline import load_trained_model

from utils.scriptutils.parser import add_predefined_arguments
from utils.commonutils.config import load_config
from utils.plotutils.plotconfig import configure_matplotlib
from utils.plotutils import performance_mpl as perfplot_mpl

configure_matplotlib()

warnings.filterwarnings(
    "ignore", "None of the inputs have requires_grad=True. Gradients will be None"
)


if __name__ == "__main__":
    parser = ArgumentParser(
        "Plot the triplet performance as a function of the minimal triplet score"
    )
    add_predefined_arguments(
        parser, ["pipeline_config", "test_dataset_name", "output_path"]
    )
    parsed_args = parser.parse_args()
    pipeline_config_path: str = parsed_args.pipeline_config
    test_dataset_name: str = parsed_args.test_dataset_name
    output_path: str | None = parsed_args.output_path

    gnn_model = load_trained_model(path_or_config=pipeline_config_path, step="gnn")
    assert isinstance(gnn_model, TripletInteractionGNN), (
        "This script only support the `TripletInteraction GNN for now, "
        f"but the model is {type(gnn_model).__name__}"
    )

    results = perfplot_mpl.plot_triplet_performance(
        gnn_model,
        max_n_events=200,
        edge_score_cut=load_config(pipeline_config_path)["track_building"][
            "edge_score_cut"
        ],
        triplet_score_cuts=[
            0.1,
            0.2,
            0.3,
            0.35,
            0.4,
            0.45,
            0.5,
            0.7,
            0.9,
        ],
        output_path=output_path,
    )
