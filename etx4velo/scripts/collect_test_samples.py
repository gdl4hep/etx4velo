#!/usr/bin/env python3
"""A script that generates the ``test_samples.yaml`` file that defines the test samples
used in this repository.
"""
from argparse import ArgumentParser
from utils.commonutils.ctests import collect_test_samples


if __name__ == "__main__":
    parser = ArgumentParser(
        "Generate `test_samples.yaml` that defines the test samples."
    )
    parser.add_argument(
        "-i",
        "--indir",
        help=(
            "Directory where the test samples are located. "
            "They are generated by the XDIGI2CSV repository."
        ),
        required=False,
    )
    parser.add_argument(
        "-n",
        "--number-of-events",
        help="Number of events for each test sample.",
        type=int,
        default=1000,
        required=False,
    )
    parser.add_argument(
        "-t",
        "--test_config",
        help=(
            "Supplementary test sample YAML configuration file "
            "to merge to the configuration produced by this script."
        ),
        required=False,
    )
    parser.add_argument(
        "-o",
        "--output",
        help="Path where the test sample YAML configuration is saved.",
        required=False,
    )
    parser.parse_args()
    parsed_args = parser.parse_args()
    indir: str = parsed_args.indir
    n_events: int = parsed_args.number_of_events
    supplementary_test_config_path: str | None = parsed_args.test_config
    output_path: str = parsed_args.output

    collect_test_samples(
        reference_directory=indir,
        output_path=output_path,
        n_events=n_events,
        supplementary_test_config_path=supplementary_test_config_path,
    )
