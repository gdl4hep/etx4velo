#!/bin/bash

#SBATCH --job-name=etx4velo
#SBATCH --nodes=1
#SBATCH --time=43200
#SBATCH --output=%x-%j.out
#SBATCH --error=%x-%j.err
#SBATCH --gpus=a100_7g.80gb:1
#SBATCH --cpus-per-gpu=16

eval "$(conda shell.bash hook)"
source setup/setup.sh
conda activate etx4velo_env                           # Activate python environment
jupyter-lab --no-browser --port 8898              # Jupyter lab
