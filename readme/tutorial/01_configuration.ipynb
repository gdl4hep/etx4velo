{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# ETX4VELO Configuration\n",
    "\n",
    "Welcome to this second section regarding the configuration of the ETX4VELO repository.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Repository Organisation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The root directory of the ETX4VELO repository contains several folders:\n",
    "- `etx4velo`: the main repository that contains the models, pipeline configurations,\n",
    "notebooks, etc.\n",
    "- `readme`: the README markdown files used in the documentation website.\n",
    "- `docs`: the source files to build the documentation with sphinx.\n",
    "- `setup`: the environment and configuration files.\n",
    "\n",
    "The main folder is `etx4velo`, where you can find the following folders\n",
    "- `pipeline`: is the heart of ETX4VELO, containing all the packages and models.\n",
    "- `notebooks`: contains Notebooks for interactively run trainings and evaluations.\n",
    "- `pipeline_configs`: contains all the pipeline configurations for training and inference.\n",
    "- `scripts`: contains scripts to run some steps of the pipeline from the command line.\n",
    "- `snakefiles`: Snakemake files to run automated and reproducible evaluation of\n",
    "the ETX4VELO pipeline.\n",
    "- `analyses`: random notebooks I use to debug or understand problems\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Setup file\n",
    "\n",
    "First, source the `setup/setup.sh` file.\n",
    "```bash\n",
    "source setup/setup.sh\n",
    "```\n",
    "This defines the environment variable `ETX4VELO_REPO`, containing the absolute path\n",
    "to this repository, and add `montetracko`, `etx4velo`, `etx4velo/pipeline` to \n",
    "the `PYTHONPATH` environment variable."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once this file is sourced, you may launch `jupyter-lab`\n",
    "```bash\n",
    "cd etx4velo\n",
    "jupyter-lab --port 8889 --no-browser\n",
    "```\n",
    "and open this notebook on your internet browser.\n",
    "\n",
    "You can inspect your environment variables and the PYTHONPATH content:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import sys\n",
    "from pprint import pprint\n",
    "\n",
    "print(\"ETX4VELO_REPO environment variable:\", os.environ[\"ETX4VELO_REPO\"])\n",
    "\n",
    "print(\"\\nPYTHONPATH content:\")\n",
    "pprint(sys.path)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Configuration Files"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To properly use the ETX4VELO repository, there is still a few things you need to do.\n",
    "\n",
    "First edit the `setup/common_config.yaml` file to your liking, more particularly\n",
    "the `directories` section:\n",
    "```yaml\n",
    "directories:\n",
    "  # Directory where the processed files are saved. You may need space to store this folder.\n",
    "  data_directory: /scratch/acorreia/data\n",
    "  # Directory where the model parameters are saved during training\n",
    "  artifact_directory: artifacts\n",
    "  # The plots and reports of a given experiment are saved under this folder\n",
    "  performance_directory: output\n",
    "  # Directory that contains the reference (test) samples\n",
    "  reference_directory: /scratch/acorreia/reference_samples\n",
    "  # Directory that contains other figures, used for presentations for instance\n",
    "  analysis_directory: output/analysis\n",
    "  # Directory that contains the exported model\n",
    "  export_directory: model_export\n",
    "```\n",
    "\n",
    "The relative paths are expressed w.r.t. the `etx4velo` folder of the repository.\n",
    "\n",
    "The configuration that you are likely to change are:\n",
    "- `data_directory`\n",
    "- `reference_directory`: change it to where you extracted the `reference_samples_tutorial.tar.lz4` archive"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For convenience, these directories can be retrieved using the `cdirs` object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from utils.commonutils.config import cdirs\n",
    "\n",
    "for dirtype in [\"data\", \"artifact\", \"performance\", \"reference\", \"analysis\", \"export\"]:\n",
    "    attribute_name = f\"{dirtype}_directory\"\n",
    "    print(f\"{f'cdirs.{attribute_name}':<30}:\", getattr(cdirs, attribute_name))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Collect Test Samples"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Please move to the `etx4velo` directory and run the following script\n",
    "\n",
    "```bash\n",
    "./scripts/collect_test_samples.py\n",
    "```\n",
    "which produces, the  `etx4velo/test_samples.yaml` file, which is the configuration\n",
    "for the test samples. The test samples are collected by navigating through the folders\n",
    "in `cdirs.reference_directory`.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Pipeline Configuration"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The pipeline configurations are stored in in the `pipeline_configs` directory.\n",
    "Let's focus on the `pipeline_configs.yaml` configuration.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "config_path = os.path.join(cdirs.repository, \"etx4velo\", \"pipeline_configs\", \"example.yaml\")\n",
    "print(\"config_path:\", config_path)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To load the configuration, you should always use the `load_config` function,\n",
    "because it alters the configuration for convenience."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from utils.commonutils.config import load_config\n",
    "\n",
    "config = load_config(config_path)\n",
    "assert config == load_config(config) # pass-through if it already a dictionary, for convenience!\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pprint(config)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The configuration is essentially a dictionary of dictionaries.\n",
    "It is divided into several sections, corresponding the pipeline steps."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Configuration sections:\", list(config.keys()))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First look at the `common` section:\n",
    "```yaml\n",
    "common:\n",
    "  experiment_name: example # Optional: this is automatically set to the name of the config file\n",
    "  # Name of the test datasets to use (defined in `evaluation/test_samples.yaml`)\n",
    "  test_dataset_names:\n",
    "  - minbias-sim10b-xdigi_v2.4_1496\n",
    "  - minbias-sim10b-xdigi_v2.4_1498\n",
    "  detector: velo # default to the first entry in `detectors` in `common_config.yaml`\n",
    "```\n",
    "which defines:\n",
    "- the `experiment_name`, set to the name of the configuration file by `load_config`!\n",
    "- the test dataset names made available to the pipeline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We'll go over the next sections of the configuration in subsequent parts of this tutorial."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
