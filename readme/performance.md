
# Current Performance in the Velo

The performance evaluation of the GNN-based pipeline depicted below does not account
for inference time.
Consequently, while the GNN-based pipeline demonstrates superior physics performance
compared to the default track finding algorithm within the Velo subdetector,
it is imperative to acknowledge that substantial efforts are required
to tailor the pipeline to meet the demanding high-throughput constraints
required for the first-level trigger of the LHCb experiment.

## $p$-$p$ collisions with spillover

The default classical algorithm in Allen have the performance detailed
by the 2 tables below.

| Categories                  | Efficiency | Average efficiency | Clone rate | Average hit purity | Average hit efficiency |
| :-------------------------- | :--------- | :----------------- | :--------- | :----------------- | :--------------------- |
| Velo                        | 94.30%     | 94.43%             | 3.85%      | 99.36%             | 93.76%                 |
| Long                        | 99.12%     | 99.18%             | 2.66%      | 99.64%             | 96.37%                 |
| Velo, no electrons          | 98.22%     | 98.31%             | 3.10%      | 99.66%             | 95.36%                 |
| Velo only, no electrons     | 96.84%     | 96.98%             | 3.84%      | 99.50%             | 93.89%                 |
| Velo only, only electrons   | 67.91%     | 67.49%             | 10.27%     | 97.35%             | 79.21%                 |
| Long, only electrons        | 97.11%     | 97.29%             | 4.25%      | 97.67%             | 95.24%                 |
| Velo only, from strange     | 93.53%     | 93.77%             | 5.60%      | 99.36%             | 90.05%                 |
| Velo only, from $K^{0}_{S}$ | 94.83%     | 94.97%             | 5.40%      | 99.54%             | 90.25%                 |
| Long, from $K^{0}_{S}$      | 98.52%     | 98.80%             | 2.55%      | 99.62%             | 96.26%                 |

| Categories | # ghosts | # tracks  | Ghost rate |
| :--------- | :------- | :-------- | :--------- |
| Everything | 27,209   | 1,245,631 | 2.18%      |

The etx4velo algorithm performances are given by the 2 tables below.

| Categories                  | Efficiency | Average efficiency | Clone rate | Average hit purity | Average hit efficiency |
| :-------------------------- | :--------- | :----------------- | :--------- | :----------------- | :--------------------- |
| Velo                        | 97.26%     | 97.22%             | 1.66%      | 99.73%             | 98.02%                 |
| Long                        | 99.49%     | 99.46%             | 1.35%      | 99.83%             | 98.74%                 |
| Velo, no electrons          | 98.80%     | 98.78%             | 0.95%      | 99.89%             | 98.65%                 |
| Velo only, no electrons     | 97.86%     | 97.85%             | 1.02%      | 99.82%             | 98.32%                 |
| Velo only, only electrons   | 86.69%     | 86.11%             | 4.97%      | 98.99%             | 93.88%                 |
| Long, only electrons        | 99.22%     | 99.22%             | 7.31%      | 98.46%             | 96.79%                 |
| Velo only, from strange     | 95.25%     | 95.12%             | 1.77%      | 99.64%             | 96.05%                 |
| Velo only, from $K^{0}_{S}$ | 96.10%     | 95.95%             | 1.72%      | 99.75%             | 96.04%                 |
| Long, from $K^{0}_{S}$      | 98.95%     | 98.99%             | 0.80%      | 99.89%             | 98.83%                 |

| Categories | # ghosts | # tracks  | Ghost rate |
| :--------- | :------- | :-------- | :--------- |
| Everything | 10,151   | 1,258,270 | 0.81%      |

5,000 events where used for this evaluation.

## $Pb$-$Pb$ collisions

The default classical algorithm in Allen have the performance detailed
by the 2 tables below.

| Categories                  | Efficiency | Average efficiency | Clone rate | Average hit purity | Average hit efficiency |
| :-------------------------- | :--------- | :----------------- | :--------- | :----------------- | :--------------------- |
| Velo                        | 91.51%     | 93.36%             | 4.03%      | 98.08%             | 92.88%                 |
| Long                        | 97.60%     | 98.64%             | 2.88%      | 98.59%             | 95.70%                 |
| Velo, no electrons          | 96.03%     | 97.62%             | 3.51%      | 98.43%             | 94.30%                 |
| Velo only, only electrons   | 61.63%     | 63.48%             | 9.17%      | 95.30%             | 78.49%                 |
| Long, only electrons        | 91.70%     | 94.65%             | 3.76%      | 96.05%             | 94.55%                 |
| Velo only, from $K^{0}_{S}$ | 83.34%     | 90.19%             | 5.34%      | 97.57%             | 88.39%                 |
| Long, from $K^{0}_{S}$      | 92.27%     | 95.29%             | 2.91%      | 98.11%             | 94.53%                 |

| Categories | # ghosts | # tracks  | Ghost rate |
| :--------- | :------- | :-------- | :--------- |
| Everything | 110,064  | 1,022,995 | 10.76%     |

The etx4velo algorithm performances are given by the 2 tables below.

| Categories                  | Efficiency | Average efficiency | Clone rate | Average hit purity | Average hit efficiency |
| :-------------------------- | :--------- | :----------------- | :--------- | :----------------- | :--------------------- |
| Velo                        | 95.50%     | 96.42%             | 2.37%      | 99.70%             | 96.49%                 |
| Long                        | 98.90%     | 99.24%             | 1.78%      | 99.79%             | 98.09%                 |
| Velo, no electrons          | 97.69%     | 98.35%             | 1.63%      | 99.82%             | 97.62%                 |
| Velo only, only electrons   | 80.31%     | 82.15%             | 6.75%      | 99.12%             | 88.07%                 |
| Long, only electrons        | 98.38%     | 98.68%             | 6.51%      | 98.64%             | 95.78%                 |
| Velo only, from $K^{0}_{S}$ | 88.66%     | 92.75%             | 3.50%      | 99.56%             | 90.15%                 |
| Long, from $K^{0}_{S}$      | 96.47%     | 97.79%             | 1.60%      | 99.75%             | 96.71%                 |

| Categories | # ghosts | # tracks | Ghost rate |
| :--------- | :------- | :------- | :--------- |
| Everything | 9,617    | 983,172  | 0.98%      |


1,000 events stored under `LFN:/lhcb/MC/Upgrade/XDIGI/00165652/0000/00165652_00000699_1.xdigi`
where used for this evaluation.
