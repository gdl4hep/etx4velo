# Setup Guide

This page provides instructions on setting up this repository, both for the initial setup
and for subsequent sessions.


## Initial Setup

For the first-time setup, ensure you have a functional mamba (or conda) installation.
To optimize environment resolution and package installation speed, it is recommended to use mamba.
Follow the [official instructions](https://github.com/conda-forge/miniforge#unix-like-platforms-mac-os--linux).
to install mamba.

Then,
```bash
# 1. Clone the repository
git clone ssh://git@gitlab.cern.ch:7999/gdl4hep/etx4velo.git
# Using https: git clone https://gitlab.cern.ch/gdl4hep/etx4velo.git
cd etx4velo

# 2. Switch to the `dev` branch
git switch dev

# 3. Fetch the `montetracko` git submodule
git submodule update --init

# 4. Setup the Conda environment
mamba env create -f setup/gpu_enviroment.yaml # with CUDA
# or CPU only: mamba env create -f setup/cpu_enviroment.yaml

# 5. Optionally, activate jupyter widgets
jupyter nbextension enable --py widgetsnbextension
```


Additionally, you have the flexibility to customize the paths to various output
directories according to your preferences. Modify these paths in the
`setup/common_config.yaml` file, where relative paths are based on the `etx4velo`
folder within this repository.

## Subsequent Sessions

For each new session, follow these steps to prepare your environment:

1. Source the `setup/setup.sh` file, which accomplishes the following:
    - Defines the environment variable `ETX4VELO_REPO`, containing the absolute path
    to this repository.
    - Adds `montetracko`, `etx4velo` and `etx4velo/pipeline` to the `PYTHONPATH`.
    ```bash
    source setup/setup.sh
    ```
2. Activate the conda environment
    ```bash
    conda activate etx4velo_env
    ```

3. If desired, start a Jupyter Lab server:
    ```bash
    jupyter-lab
    ```

## Next Steps

you can refer to the [test sample guide](./2_test_samples.md) to set up
the test samples, or directly processed to the [training guide](../guide/3_training.md)
for instructions on running a training of a pipeline.
