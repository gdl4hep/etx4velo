# Test Samples

We provide "reference" samples, which are accessible from my EOS space.


## Downloading

You can acquire the reference samples archive using `xrdcp` and then extract its contents:

```bash
xrdcp root://eoslhcb.cern.ch//eos/lhcb/user/a/anthonyc/tracking/data/data_validation/v2.4/reference_samples.tar.lz4 .
lz4 -d reference_samples.tar.lz4 -c | tar xvf -
```

Afterwards, you should modify the `reference_directory` entry in `setup_common_config.yaml`.
o reflect the actual location of the `reference_samples` directory.

## Sample Contents

The `reference_samples` folder encompasses multiple subfolders,
each corresponding to an individual reference (or test) sample.
Within each folder, such as `minbias-sim10b-xdigi_v2.4_1498`, you'll find
- `dumped_mdf.mdf` and `geometry`: These files consist of the MDF file and geometry files,
which can be employed as input for Allen.
- `persistence_velo_tracks`: this directory houses the Velo tracks constructed by Allen.
- `xdigi2csv`:  It contains the `.parquet.lz4` files,
which include the Velo, UT, SciFi hits, as well as true particle information.

All these files were generated
using the [(X)DIGI2CSV repository](https://gitlab.cern.ch/gdl4hep/xdigi2csv/-/tree/master/reference_samples).

The naming convention for a test sample follows this pattern:
`{production_name}_{version}_{subfolder(s)}`
where `{version}` and `{subfolder}` denote the version and subfolder name
in which the sample was produced by the (X)DIGI2CSV repository.
This ensures that these samples are distinct from the training and validation sets.

## Process the Test Samples

To gather, preprocess, and assess the tracking performance on the test samples,
execute the following commands:
```bash
# If not already, set up the environment
source setup/setup.sh
conda activate etx4velo_env
cd etx4velo

snakemake -c1 -p evaluate_allen_all
```
Here, the `-c1` option specifies the maximum number of concurrently executed jobs,
with the setting of 1 indicating one job at a time. You can adjust this number as needed;
for instance, `-c5` would allow a maximum of five jobs to run concurrently.
