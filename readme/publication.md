# Conferences and Publications

## Connecting The Dots 2023

The presentation at the CTD 2023 can be found [here](https://indico.cern.ch/event/1252748/contributions/5521484/).
The focus was track-finding in the Velo, using a GNN-based pipeline based on
the original Exa.TrkX pipeline, with extra steps to reconstruct tracks that share hits.

The pipelines used in this presentation are:
- `focal-loss-nopid-triplets-embedding-3-withspillover-new`: Embedding and GNN with $d^2_\text{max} = 0.010$
- `focal-loss-nopid-triplets-embedding-3-withspillover-d2max0.02`: GNN with $d^2_\text{max} = 0.020$

## Journées de Rencontre Jeunes Chercheurs 2023

The presentation at JRJC 2023 can be found [here](https://indico.in2p3.fr/event/30000/contributions/128744/).
The focus was on the different steps of the pipeline, and on our first approach to its implementation in Allen.
