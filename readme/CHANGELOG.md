# CHANGELOG

## v0.2

### Repository

- Clean up and refactor modules
- Implement detectors for SciFi training
- Define systematic way of exporting models to ONNX
- Define centralised way of loading embedding and GNN models
(with `load_trained_model` and `instantiate_model_for_training`)
- Be able to do transfer learning
- Use a common script for training the embedding and the GNN networks
- Add APIDOC to the documentation website


### Embedding

- Be able to train and apply successive embeddings + kNNs, stacking the edge indices
- Be able to change the direction of edges
- Be able to train and apply embedding only on given planes
- Define `particle_requirement`, `query_particle_requirement` and `target_requirement`
- Move various graph utilities in `utils/graphutils`
- Remove any reference to "metric_learning" ; only use "embedding"

### GNN

- Define `TripletGNNBase`, the base class of all triplet-based GNNs
- Remove useless preprocessing of events (i.e., "correct shared hits"-related functions)
- Move triplet-building utilities to `utils/graphutils`
- Define `EdgeBasedGNN` and `SciFiTripletInteractionGNN` triplet-based GNNs
