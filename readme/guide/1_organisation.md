# Organisation

This guide will help you navigate through the various folders and components within it.

## Main Repository Folder: `etx4velo`

The primary folder of this repository, etx4velo, serves as the hub for models,
notebooks, and scripts related to our project.

Here's a brief overview of what you'll find:
- `pipeline`: This Python package is the heart of our project.
It defines all the PyTorch models and includes utilities for data processing and plotting.
- `pipeline_configs`: In this folder, you'll discover YAML files.
Each YAML file corresponds to a comprehensive configuration for one training and
inference pipeline.
- `notebooks`: This folder contains Jupyter notebooks, to train and test the full pipeline
in an interactive manner.
- `scripts`: If you prefer script-based execution, this folder offers scripts
for every step of the pipeline, from data preprocessing to track finding evaluations.
- `artifact_archive`: This folder is an archive of important runs: model checkpoints, reports and configurations.
- `snakefiles`: In this directory, you'll discover Snakemake files.
These files include rules designed to automatically generate plots
used in presentations and conferences, ensuring reproducibility.

## Additional repository Folders

Beyond the `etx4velo` folder, there are a few other directories:
- `docs`: this folder holds documentation related to our project.
- `readme`: In this folder, you'll find essential setup guides to get you started
- `montetracko`: this folder is a Git submodule containing
the [montetracko library](https://gitlab.cern.ch/gdl4hep/montetracko/).
- `setup`: here, you'll find various files crucial for setting up the repository.

## Using Notebooks and Snakemake

The Jupyter notebooks, located in etx4velo/notebooks, are essential for training and testing.
Additionally, we employ the
The [Snakemake](https://snakemake.readthedocs.io/en/stable/) workflow management system
to generate figures in a reproducible manner from trained models.
