# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))


# -- Project information -----------------------------------------------------

project = "etx4velo"
copyright = "2023, Fotis Giasemis, Anthony Correia"
author = "Anthony Correia, Fotis Giasemis"

# The full version, including alpha/beta/rc tags
release = "v0.1"


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx.ext.autodoc",  # aut-documentation of python code
    "sphinx.ext.viewcode",  # be able to see code directly in the documentation website
    "sphinx.ext.napoleon",  # numpy doc style
    # "sphinx.ext.coverage",
    "sphinx_autodoc_typehints",  # to read the python typehints
    "sphinx.ext.mathjax",  # for math support
    "sphinx-mathjax-offline",  # for using mathjax in offline mode
    "myst_parser",  # to parse markdown files
    "sphinx_multiversion",  # Multi-versioning
]

autoapi_dirs = ["../etx4velo"]

autodoc_default_options = {
    "members": True,
    "undoc-members": False,
    "private-members": False,
}

autodoc_mock_imports = [
    "tqdm",
    "scipy",
    # "numpy",
    "pandas",
    "torch",
    "cupy",
    "cudf",
    "sklearn",
    "joblib",
    "numba",
    "faiss",
    "cugraph",
    "pytorch_lightning",
    "torch_geometric",
    "torch_scatter",
    "torchvision",
    "matplotlib",
    "torchinfo",
    "onnx",
    "uncertainties",
]

exclude_patterns = []

source_suffix = [".rst", ".md"]
myst_enable_extensions = [
    "dollarmath",
    "amsmath",
    "tasklist",
]
# myst_dmath_double_inline = True
myst_heading_anchors = 3
myst_number_code_blocks = ["typescript"]
myst_update_mathjax = False

# Branches to include in the documentation
smv_branch_whitelist = r'^(main)$'  # Only main
smv_tag_whitelist = r'^(v\d+\.\d+|ctd2023)$'


# -- Options for HTML output -------------------------------------------------

html_theme = "sphinx_book_theme"
html_static_path = ["_static"]
html_theme_options = {
    "repository_url": "https://gitlab.cern.ch/gdl4hep/etx4velo/-/tree/dev",
    "use_repository_button": True,
    "path_to_docs": "docs/source",
    "use_edit_page_button": False,
    "home_page_in_toc": True,
    "repository_branch": True,
    "show_toc_level": 2,
}

html_title = ""
html_logo = "_static/png/logo.png"
html_favicon = "_static/png/logo_squared.png"

if (
    "SPHINX_MULTIVERSION_ACTIVATED" in os.environ
    and os.environ["SPHINX_MULTIVERSION_ACTIVATED"] == "true"
):
    templates_path = [
        "_templates",
    ]

html_css_files = [
    "css/readthedocs-doc-embed.css",
    "css/local.css",
    "css/badge_only.css",
]
