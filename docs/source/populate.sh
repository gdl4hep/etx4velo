#!/bin/bash
# Note: here I copy the MARKDOWN files
# This is extremely ugly but this is the only way I found in order
# for the interlinks to work properly
currentdir=$(dirname "$0")

rm $currentdir/*.md || true
rm $currentdir/*/*.md || true

echo "current directory: $currentdir"

if [ -z ${ETX4VELO_REPO+x} ] ; then
    echo "Error: please run `source setup/setup.sh`"
    exit 1
fi

echo "REPOSITORY: $ETX4VELO_REPO"

for folder_or_file in $ETX4VELO_REPO/readme/*; do
    basename="${folder_or_file##*/}"

    rm -rf $currentdir/$basename
    cp -r $folder_or_file $currentdir/
done
