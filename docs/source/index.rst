.. etx4velo documentation master file, created by
   sphinx-quickstart on Sun Sep 17 18:07:08 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

ETX4VELO
========

The ETX4VELO repository is an attempt in using Graph Neural Networks (GNN)
to perform track finding at LHCb.
It is based on the pipeline developed by
`the Exa.TrkX collaboration <https://github.com/HSF-reco-and-software-triggers/Tracking-ML-Exa.TrkX/tree/master/Pipelines/TrackML_Example>`_.
It has undergone refinements tailored to the forwardness nature of LHCb
and the specific track topology of interest.

The pipeline was presented

* at the `WP2 reconstruction meeting on the 12th of September, 2023 <https://indico.cern.ch/event/1299280/#5-enhancements-in-etx4velo-imp>`_.
* at the `CTD 2023 workshop <https://indico.cern.ch/event/1252748/contributions/5521484/>`_.

The trained weights of the various models can be found `here <https://cernbox.cern.ch/s/HKl8gHSCF57hklN>`_.

.. DANGER::
   Please be aware that the ETX4VELO repository currently lacks flexibility
   in its pipeline configuration.
   As of now, it does not support the ability to define or remove steps
   within the pipeline.
   While you can customize most of the network parameters by modifying the pipeline
   configuration YAML file, please note that not all parameters are guaranteed
   to function as expected.


.. note::
   If you are looking for a swift introduction to a GNN-based pipeline for track finding,
   you might find the `Exa.TrkX guide <https://github.com/HSF-reco-and-software-triggers/Tracking-ML-Exa.TrkX/blob/master/Examples/TrackML_Quickstart/run_quickstart.ipynb>`_
   to be a more suitable resource.
   Alternatively, a guide to train our pipeline is available under :doc:`guide/3_training`.


.. toctree::
   :hidden:

   performance.md
   publication.md


.. toctree::
   :caption: Setting-up the Repository
   :glob:
   :hidden:

   setup/*

.. toctree::
   :caption: Guides
   :glob:
   :hidden:

   guide/*


.. toctree::
   :caption: Tutorial Series
   :glob:
   :hidden:

   tutorial/*


.. toctree::
   :caption: APIDOC
   :hidden:

   apidoc/pipeline
   apidoc/scripts
   CHANGELOG


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
