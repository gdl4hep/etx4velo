pipeline package
================

.. automodule:: pipeline
   :members:
   :show-inheritance:

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   pipeline.Preprocessing
   pipeline.Processing
   pipeline.Embedding
   pipeline.GNN
   pipeline.TrackBuilding
   pipeline.Evaluation
   pipeline.utils
