scripts/trackfactory folder
===========================

.. automodule:: scripts.trackfactory
   :members:
   :undoc-members:
   :show-inheritance:

trackfactory.analyse\_long\_electron\_clones.py script
------------------------------------------------------

.. automodule:: scripts.trackfactory.analyse_long_electron_clones
   :members:
   :undoc-members:
   :show-inheritance:

trackfactory/plot\_crossing\_tracks.py script
------------------------------------------

.. automodule:: scripts.trackfactory.plot_crossing_tracks
   :members:
   :undoc-members:
   :show-inheritance:

trackfactory/plot\_shared\_electron\_tracks.py script
-----------------------------------------------------

.. automodule:: scripts.trackfactory.plot_shared_electron_tracks
   :members:
   :undoc-members:
   :show-inheritance:


trackfactory.dfutils module
---------------------------

.. automodule:: scripts.trackfactory.dfutils
   :members:
   :undoc-members:
   :show-inheritance: