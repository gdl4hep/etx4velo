scripts folder
==============

.. automodule:: scripts
   :members:
   :undoc-members:
   :show-inheritance:

Subfolders
----------

.. toctree::
   :maxdepth: 4

   scripts.evaluation
   scripts.plotfactory
   scripts.trackfactory

collect\_test\_samples.py script
----------------------------------

.. automodule:: scripts.collect_test_samples
   :members:
   :undoc-members:
   :show-inheritance:

preprocess\_test\_sample.py script
----------------------------------

.. automodule:: scripts.preprocess_test_sample
   :members:
   :undoc-members:
   :show-inheritance:

process\_test\_sample.py script
-------------------------------

.. automodule:: scripts.process_test_sample
   :members:
   :undoc-members:
   :show-inheritance:

build\_graph\_using\_embedding.py script
----------------------------------------

.. automodule:: scripts.build_graph_using_embedding
   :members:
   :undoc-members:
   :show-inheritance:

train\_model.py script
----------------------

.. automodule:: scripts.train_model
   :members:
   :undoc-members:
   :show-inheritance:

build\_tracks.py script
-----------------------

.. automodule:: scripts.build_tracks
   :members:
   :undoc-members:
   :show-inheritance:

export\_model\_to\_onnx.py script
---------------------------------

.. automodule:: scripts.export_model_to_onnx
   :members:
   :undoc-members:
   :show-inheritance:
