pipeline.Evaluation package
===========================

.. automodule:: pipeline.Evaluation
   :members:
   :undoc-members:
   :show-inheritance:

pipeline.Evaluation.matching module
-----------------------------------

.. automodule:: pipeline.Evaluation.matching
   :members:
   :undoc-members:
   :show-inheritance:

pipeline.Evaluation.plotting module
-----------------------------------

.. automodule:: pipeline.Evaluation.plotting
   :members:
   :undoc-members:
   :show-inheritance:

pipeline.Evaluation.reporting module
------------------------------------

.. automodule:: pipeline.Evaluation.reporting
   :members:
   :undoc-members:
   :show-inheritance:
