pipeline.GNN package
====================

.. automodule:: pipeline.GNN
   :members:
   :show-inheritance:

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   pipeline.GNN.models


pipeline.GNN.triplet\_gnn\_base module
--------------------------------------

.. automodule:: pipeline.GNN.triplet_gnn_base
   :members:
   :show-inheritance:

pipeline.GNN.perfect\_gnn module
--------------------------------

.. automodule:: pipeline.GNN.perfect_gnn
   :members:
   :show-inheritance:

pipeline.GNN.gnn\_validation module
-----------------------------------

.. automodule:: pipeline.GNN.gnn_validation
   :members:
   :show-inheritance:

pipeline.GNN.gnn\_plots module
------------------------------

.. automodule:: pipeline.GNN.gnn_plots
   :members:
   :show-inheritance:
