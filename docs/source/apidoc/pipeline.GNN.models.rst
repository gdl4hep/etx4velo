pipeline.GNN.models package
===========================

.. automodule:: pipeline.GNN.models
   :members:
   :show-inheritance:

pipeline.GNN.models.edge\_based\_gnn module
-------------------------------------------

.. automodule:: pipeline.GNN.models.edge_based_gnn
   :members:
   :show-inheritance:

pipeline.GNN.models.incremental\_triplet\_interaction\_gnn module
-----------------------------------------------------------------

.. automodule:: pipeline.GNN.models.incremental_triplet_interaction_gnn
   :members:
   :show-inheritance:

pipeline.GNN.models.scifi\_triplet\_interaction\_gnn module
-----------------------------------------------------------

.. automodule:: pipeline.GNN.models.scifi_triplet_interaction_gnn
   :members:
   :show-inheritance:

pipeline.GNN.models.triplet\_interaction\_gnn module
----------------------------------------------------

.. automodule:: pipeline.GNN.models.triplet_interaction_gnn
   :members:
   :show-inheritance:
