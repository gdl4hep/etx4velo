pipeline.Processing package
===========================

.. automodule:: pipeline.Processing
   :members:
   :undoc-members:
   :show-inheritance:

pipeline.Processing.compute module
----------------------------------

.. automodule:: pipeline.Processing.compute
   :members:
   :undoc-members:
   :show-inheritance:

pipeline.Processing.modulewise\_edges module
--------------------------------------------

.. automodule:: pipeline.Processing.modulewise_edges
   :members:
   :undoc-members:
   :show-inheritance:

pipeline.Processing.planewise\_edges module
-------------------------------------------

.. automodule:: pipeline.Processing.planewise_edges
   :members:
   :undoc-members:
   :show-inheritance:

pipeline.Processing.processing module
-------------------------------------

.. automodule:: pipeline.Processing.processing
   :members:
   :undoc-members:
   :show-inheritance:

pipeline.Processing.run\_processing module
------------------------------------------

.. automodule:: pipeline.Processing.run_processing
   :members:
   :undoc-members:
   :show-inheritance:

pipeline.Processing.sortedwise\_edges module
--------------------------------------------

.. automodule:: pipeline.Processing.sortedwise_edges
   :members:
   :undoc-members:
   :show-inheritance:

pipeline.Processing.splitting module
------------------------------------

.. automodule:: pipeline.Processing.splitting
   :members:
   :undoc-members:
   :show-inheritance:
