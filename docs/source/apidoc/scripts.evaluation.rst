scripts/evaluation folder
=========================

.. automodule:: scripts.evaluation
   :members:
   :undoc-members:
   :show-inheritance:


plot\_training\_metrics.py script
---------------------------------

.. automodule:: scripts.evaluation.plot_training_metrics
   :members:
   :undoc-members:
   :show-inheritance:

plot\_embedding\_edge\_performance.py script
--------------------------------------------

.. automodule:: scripts.evaluation.plot_embedding_edge_performance
   :members:
   :undoc-members:
   :show-inheritance:

plot\_embedding\_best\_tracking\_performance.py script
------------------------------------------------------

.. automodule:: scripts.evaluation.plot_embedding_best_tracking_performance
   :members:
   :undoc-members:
   :show-inheritance:

plot\_embedding\_edge\_plane\_diff.py script
--------------------------------------------

.. automodule:: scripts.evaluation.plot_embedding_edge_plane_diff
   :members:
   :undoc-members:
   :show-inheritance:

plot\_embedding\_n\_neigbhours.py script
----------------------------------------

.. automodule:: scripts.evaluation.plot_embedding_n_neigbhours
   :members:
   :undoc-members:
   :show-inheritance:


plot\_gnn\_edge\_performance.py script
--------------------------------------

.. automodule:: scripts.evaluation.plot_gnn_edge_performance
   :members:
   :undoc-members:
   :show-inheritance:

plot\_gnn\_triplet\_performance.py script
-----------------------------------------

.. automodule:: scripts.evaluation.plot_gnn_triplet_performance
   :members:
   :undoc-members:
   :show-inheritance:

plot\_gnn\_best\_tracking\_performance.py script
------------------------------------------------

.. automodule:: scripts.evaluation.plot_gnn_best_tracking_performance
   :members:
   :undoc-members:
   :show-inheritance:

evaluate\_allen\_on\_test\_sample.py script
-------------------------------------------

.. automodule:: scripts.evaluation.evaluate_allen_on_test_sample
   :members:
   :undoc-members:
   :show-inheritance:

evaluate\_etx4velo.py script
--------------------------

.. automodule:: scripts.evaluation.evaluate_etx4velo
   :members:
   :undoc-members:
   :show-inheritance:

compare\_allen\_vs\_etx4velo.py script
--------------------------------------

.. automodule:: scripts.evaluation.compare_allen_vs_etx4velo
   :members:
   :undoc-members:
   :show-inheritance: