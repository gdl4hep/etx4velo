scripts/plotfactory folder
===========================

.. automodule:: scripts.plotfactory
   :members:
   :undoc-members:
   :show-inheritance:

plot\_data\_statistics.py script
--------------------------------

.. automodule:: scripts.plotfactory.plot_data_statistics
   :members:
   :undoc-members:
   :show-inheritance:

plot\_embedding\_explanation.py script
--------------------------------------

.. automodule:: scripts.plotfactory.plot_embedding_explanation
   :members:
   :undoc-members:
   :show-inheritance:

plot\_plane\_edge\_statistics.py script
---------------------------------------

.. automodule:: scripts.plotfactory.plot_plane_edge_statistics
   :members:
   :undoc-members:
   :show-inheritance:
