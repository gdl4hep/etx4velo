#!/bin/bash

#SBATCH --job-name=etx4velo
#SBATCH --nodes=1
#SBATCH --time=28800
#SBATCH --output=%x-%j.out
#SBATCH --error=%x-%j.err
#SBATCH --gpus=a100_3g.40gb:1
#SBATCH --cpus-per-gpu=16

eval "$(conda shell.bash hook)"
source setup/setup.sh
conda activate etx4velo_env                       # Activate python environment
jupyter-lab --no-browser --port 8894              # Jupyter lab
