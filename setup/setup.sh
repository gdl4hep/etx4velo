#!/bin/bash
# path to the ROOT repository
export ETX4VELO_REPO="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"
export PYTHONPATH=$PYTHONPATH:$ETX4VELO_REPO/etx4velo
export PYTHONPATH=$PYTHONPATH:$ETX4VELO_REPO/etx4velo/pipeline
export PYTHONPATH=$PYTHONPATH:$ETX4VELO_REPO/montetracko
