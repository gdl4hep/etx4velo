"""A module to set up the PYTHONPATH and necessary environment variables.
"""
import os
import os.path as op
import sys


def setup_envvars():
    repo_dir = op.abspath(op.join(op.dirname(__file__), ".."))
    os.environ["ETX4VELO_REPO"] = repo_dir
    sys.path.insert(0, op.join(repo_dir, "etx4velo"))
    sys.path.insert(0, op.join(repo_dir, "etx4velo", "pipeline"))
    sys.path.insert(0, op.join(repo_dir, "montetracko"))
